mod album;
mod user;

use clap::{
    Parser, Subcommand, Args,
    builder::styling::{AnsiColor as Ansi, Styles}
};

const COLORED_OUTPUT: Styles = Styles::styled()
    .header(Ansi::Red.on_default().bold())
    .usage(Ansi::Red.on_default().bold())
    .literal(Ansi::Blue.on_default())
    .placeholder(Ansi::Green.on_default());

/// Wavy administration cli
#[derive(Parser, Debug)]
#[command(version, about, long_about = None, styles = COLORED_OUTPUT)]
struct ParseArgs {
    #[command(subcommand)]
    command: Commands
}

#[derive(Debug, Subcommand)]
enum Commands {
    Album {
        /// Media directory where albums will get modified
        #[arg(long, required = true)]
        media_dir: String,
        /// Media info storage database
        #[arg(long, required = true)]
        media_db: String,
        /// Temporary json files where albums info are stored for review before being commited to the media db
        #[arg(long, required = true)]
        commit_file: String,
        #[command(subcommand)]
        com: Album
    },
    User {
        /// Access database where users are stored
        #[arg(long, required = true)]
        access_db: String,
        #[command(subcommand)]
        com: User
    }
}

#[derive(Debug, Subcommand)]
enum Album {
    Add(AddAlbum),
    Commit,
    Cleanup
}

#[derive(Debug, Args)]
struct AddAlbum {
    /// Album directory containing track
    album_dir: String,
    /// Album name
    #[arg(long)]
    name: Option<String>,
    /// Author id (Uuid)
    #[arg(long)]
    author_id: Option<uuid::Uuid>,
    /// Author country code
    #[arg(long)]
    author_country_code: Option<String>,
    /// Author name
    #[arg(long)]
    author: Option<String>,
    /// Album date of release
    #[arg(long)]
    date: Option<String>
}

#[derive(Debug, Subcommand)]
enum User {
    Add(AddUser),
    GenToken {
        /// Id of user
        user_id: uuid::Uuid
    }
}

#[derive(Debug, Args)]
struct AddUser {
    /// Username
    #[arg(long)]
    name: String,
    /// Password
    // TODO: For now we only take pass from cli, we should take it directly from stdin later...
    #[arg(long)]
    pass: String,
    /// Role (possible values: admin, normal)
    #[arg(long)]
    role: String
}

#[tokio::main]
async fn main() {
    let args = ParseArgs::parse();

    let ret = match args.command {
        Commands::Album { media_dir, media_db, commit_file, com, } => match com {
            Album::Add(add) => album::add_album(media_db, media_dir, commit_file, add).await,
            Album::Commit => album::commit_albums(media_db, commit_file).await,
            Album::Cleanup => album::cleanup_pending(media_db, media_dir).await
        },
        Commands::User { access_db, com } => match com {
            User::Add(add) => user::create_user(&access_db, add).await,
            User::GenToken { user_id } => user::gen_token(&access_db, &user_id).await
        }
    };

    if let Err(e) = ret {
        eprintln!("{e}");
        std::process::exit(1);
    }
}
