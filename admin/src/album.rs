use std::{collections::VecDeque, fs::ReadDir, path::{Path, PathBuf}, sync::OnceLock};
use regex::Regex;
use rand::Rng;
use anyhow::Result;
use tokio::process::Command;
use tokio_stream::StreamExt;
use wavy_shared::storage::media::{AddAlbumStatus, Album, Track, MediaInfoStorage};

const TMP_TRANSCODE_AUDIO: &str = "/tmp/audio_transcode.mp3";
const TMP_TRANSCODE_IMAGE: &str = "/tmp/audio_transcode.jpg";

#[derive(Debug, Default)]
struct AlbumProbe {
    pub name: Option<String>,
    pub date: Option<String>,
    pub author: Option<String>,
    pub artwork: Vec<PathBuf>
}

enum FileType {
    Audio,
    Image,
    Unknown
}

// Errors are reported here to caller because nobody should access this api beside admin
pub async fn cleanup_pending(media_db_path: String, media_dir: String) -> Result<()> {
    let media_db   = MediaInfoStorage::new(&media_db_path).await?;
    let mut stream = media_db.pending_albums()
        .await;

    let mut album_dir;

    while let Some(item) = stream.next().await {
        let item = item?;

        album_dir = PathBuf::from(&media_dir);
        album_dir.push(item.id.to_string());

        if let Err(e) = tokio::fs::remove_dir_all(album_dir.to_str().unwrap()).await {
            if std::io::ErrorKind::NotFound != e.kind() {
                eprintln!("Failed removing pending album {} : {}", item.id, e);
                continue;
            }
        }

        media_db.remove_pending_album(&item.id).await?;

        println!("{} removed", item.id);
    }

    Ok(())
}

pub async fn commit_albums(media_db_path: String, commit_file: String) -> Result<()> {
    let mut commit: VecDeque<(uuid::Uuid, Album)> = serde_json::from_slice(&tokio::fs::read(&commit_file).await?)?;
    let media_db = MediaInfoStorage::new(&media_db_path).await?;

    let mut commit_left = Vec::new();
    // Finally we persist albums in db
    while let Some((album_id, album)) = commit.pop_front() {
        let ret = match media_db
            .add_album(&album_id, &album)
            .await {
            Ok(v) => v,
            Err(e) => {
                eprintln!("Skipping {} | {} | {} due to: {}", album_id, album.author, album.name, e);
                commit_left.push((album_id, album));
                continue;
            }
        };

        if let AddAlbumStatus::Exists = ret {
            eprintln!("Album already exists {} | {} | {}", album_id, album.author, album.name);
            commit_left.push((album_id, album));
            continue;
        }
    }

    tokio::fs::write(&commit_file, &serde_json::to_vec_pretty(&commit_left)?).await?;

    println!("OK");
    std::process::exit(0);
}

pub async fn add_album(media_db_path: String, media_dir: String,
                       commit_file: String, album: crate::AddAlbum)
    -> Result<()> {
    let media_dir = PathBuf::from(media_dir);
    if !media_dir.is_dir() {
        return Err(anyhow::Error::msg("Media directory is not valid"));
    }

    let media_db = MediaInfoStorage::new(&media_db_path).await?;

    // Reading all related info about album using tracks metadata...
    let mut dir  = PathBuf::new();
    dir.push(&album.album_dir);
    let album    = read_album_dir(&dir, album).await?;
    // Moving whole album to new location
    let album_id = write_album_media(&media_db, &media_dir, &album)
        .await?;

    // Reading commit file
    let read = match tokio::fs::read(&commit_file).await {
        Ok(v) => Ok(v),
        Err(e) => match e.kind() {
            std::io::ErrorKind::NotFound => Ok(b"[]".to_vec()),
            _ => Err(e)
        }
    };
    let mut commit: Vec<(uuid::Uuid, Album)> = serde_json::from_slice(&read?)?;
    commit.push((album_id, album));
    tokio::fs::write(&commit_file, serde_json::to_vec_pretty(&commit)?).await?;

    println!("{album_id}");
    std::process::exit(0);
}

async fn write_album_media(media_db: &MediaInfoStorage, media_dir: &Path, album: &Album) -> anyhow::Result<uuid::Uuid> {
    // Generate new unique Uuid for album in media dir
    let mut seed: rand::rngs::StdRng = rand::SeedableRng::from_entropy();
    let mut album_dir;
    let mut album_id;
    loop {
        album_id = uuid::Uuid::from_u128(seed.gen::<u128>());
        album_dir = PathBuf::from(media_dir);
        album_dir.push(album_id.to_string());
        match tokio::fs::create_dir(&album_dir).await {
            Err(e) => if e.kind() == std::io::ErrorKind::AlreadyExists {
                continue;
            } else {
                return Err(anyhow::Error::msg("Can't create album in media dir"));
            }
            Ok(_) => {
                media_db.add_pending_album(&album_id).await?;
                break;
            }
        }
    };

    {
        if !album.artwork.is_empty() {
            for i in 0..album.artwork.len() {
                let mut dest = PathBuf::from(&album_dir);
                dest.push(format!("artwork{}.jpg", i));
                transcode_artwork(album, &dest, Some(i)).await?;
            }
        } else {
            let mut dest = PathBuf::from(&album_dir);
            dest.push("artwork0.jpg");
            transcode_artwork(album, &dest, None).await?;
        }
    }

    // transcoding track by track then moving it to media directory
    for track in &album.tracks {
        let mut dest = PathBuf::from(&album_dir);
        dest.push(format!("{}.mp3", track.track_number));
        transcode_track(track, &dest).await?;
    }

    Ok(album_id)
}

async fn transcode_artwork(album: &Album, dest: &Path, artwork_no: Option<usize>) -> anyhow::Result<()> {
    // Should be fine since we read path before
    let dest   = dest.to_str().unwrap();
    let status = if let Some(id) = artwork_no {
        let source = album.artwork[id].to_str().unwrap();
        Command::new("ffmpeg")
            .args([
                "-y", "-i", source,
                "-map_metadata", "-1",
                TMP_TRANSCODE_IMAGE
            ])
            .status()
            .await?
    } else {
        // Should be safe as we don't allow empty tracks
        let source = album.tracks[0].location.to_str().unwrap();
        // We try to get artwork from a track, it may be embedded as video?
        Command::new("ffmpeg")
            .args([
                "-y", "-i", source,
                "-map_metadata", "-1",
                TMP_TRANSCODE_IMAGE
            ])
            .status()
            .await?
    };

    if !status.code().eq(&Some(0)) {
        return Err(anyhow::Error::msg("Artwork for album can't be read"));
    }

    tokio::fs::copy(TMP_TRANSCODE_IMAGE, dest).await?;
    
    Ok(())
}

async fn transcode_track(track: &Track, dest: &Path) -> anyhow::Result<()> {
    // Should be fine since we read path before
    let source = track.location.to_str().unwrap();
    let dest   = dest.to_str().unwrap();
    // If track is a 320kbps mp3 we just copy it
    if track.codec.to_lowercase().eq("mp3")
        && track.bitrate.is_some_and(|bitrate| bitrate == 320000)
        // CD standard sample rate
        && track.sample_rate == 44100 {
        tokio::fs::copy(source, dest).await?;
    } else {
        let status = Command::new("ffmpeg")
            .args([
                "-y",
                "-i", source,
                "-vn",
                "-b:a", "320k",
                "-ar", "44100",
                TMP_TRANSCODE_AUDIO
            ])
            .status()
            .await?;

        if !status.code().eq(&Some(0)) {
            return Err(anyhow::Error::msg(format!("Failed to transcode track at {}", source)));
        }
    
        tokio::fs::copy(TMP_TRANSCODE_AUDIO, dest).await?;
    };

    Ok(())
}

fn read_dir(dir: &Path, album: &crate::AddAlbum) -> Result<ReadDir> {
    match dir.read_dir() {
        Ok(iter) => Ok(iter),
        Err(e) => Err(anyhow::Error::msg(format!("Error reading album dir {}: {}", album.album_dir, e)))
    }
}

#[derive(PartialEq)]
enum WithDiscs {
    Nested,
    Flat,
    False
}

async fn read_album_dir(dir: &Path, album: crate::AddAlbum) -> Result<Album> {
    let mut iter       = read_dir(dir, &album)?;
    let mut with_discs = WithDiscs::False;
    iter.any(|f| f.as_ref().is_ok_and(|x| if x.file_type().is_ok_and(|t| t.is_dir())
                && x.file_name().to_str().is_some_and(|x| x.to_lowercase().starts_with("disc") || x.to_lowercase().starts_with("cd")) {
                    with_discs = if x.path().read_dir().is_ok_and(|mut d| d.next().is_none()) {
                        WithDiscs::Flat
                    } else {
                        WithDiscs::Nested
                    };
                    true
                } else {
                    false
                }
            )
        );
    iter = read_dir(dir, &album)?;

    let mut probe              = AlbumProbe::default();
    let mut tracks: Vec<Track> = Vec::new();
    while let Some(Ok(file)) = iter.next() {
        let path = file.path();
        if !path.exists() {
            return Err(anyhow::Error::msg("File for track can't be created, this is fatal."));
        }
        let path_str = match path.to_str() {
            Some(v) => v,
            None => return Err(anyhow::Error::msg("Can't convert path to string"))
        };
        let ftype = check_file_type(path_str).await?;

        match ftype {
            FileType::Image => probe.artwork.push(path),
            FileType::Audio => match get_audio_media_metadata(path_str, &mut probe).await {
                Ok(mut v) => {
                    match with_discs {
                        WithDiscs::Flat => match v.disc {
                            Some(disc_num) => {
                                v.track_number += 1000 * (disc_num - 1);
                            },
                            None => {
                                // We need to check if there are also discs in album
                                // Tricky to do using metadata so we are eyeballing here
                                for track in &tracks {
                                    if track.track_number == v.track_number {
                                        v.track_number += 1000;
                                    } else if track.track_number > v.track_number
                                        && (track.track_number - v.track_number) % 1000 == 0 {
                                        // Damn more than one disc??
                                        v.track_number = track.track_number + 1000;
                                    }
                                }
                            }
                        },
                        WithDiscs::False => (),
                        WithDiscs::Nested => unreachable!()
                    }
                    tracks.push(v)
                },
                Err(e) => return Err(e)
            },
            FileType::Unknown => if with_discs == WithDiscs::Nested && file.file_type()?.is_dir() {
                let mut disc_num = None;
                static DISC_NUMBER: OnceLock<Regex> = OnceLock::new();
                let re = DISC_NUMBER.get_or_init(|| Regex::new(r"(\d+)$").unwrap());
                if let Some(v) = re.captures(file.file_name().to_str().unwrap()) {
                    if let Some(v) = v.get(1).and_then(|x| x.as_str().parse::<u32>().ok()) {
                        disc_num = Some(v);
                    }
                }
                match disc_num {
                    Some(disc_num) => {
                        let mut iter_disc = read_dir(&file.path(), &album)?;
                        while let Some(Ok(file)) = iter_disc.next() {
                            let path     = file.path();
                            let path_str = match path.to_str() {
                                Some(v) => v,
                                None => return Err(anyhow::Error::msg("Can't convert path to string"))
                            };
                            match check_file_type(path_str).await? {
                                FileType::Image => probe.artwork.push(path),
                                FileType::Audio => {
                                    let mut track = get_audio_media_metadata(path_str, &mut probe).await?;
                                    if disc_num > 1 {
                                        track.track_number += 1000 * (disc_num - 1);
                                    }
                                    tracks.push(track);
                                },
                                _ => ()
                            }
                        }
                    }
                    None => return Err(anyhow::anyhow!("Album disc should have a number"))
                }
            }
        }
    }

    if tracks.is_empty() {
        return Err(anyhow::Error::msg("Album directory has no track"));
    }

    Ok(Album {
        name: match album.name.or(probe.name) {
            Some(v) => v,
            None => return Err(anyhow::Error::msg("Album name can't be probed neither was it supplied"))
        },
        date: album.date.or(probe.date),
        author: match album.author.or(probe.author) {
            Some(v) => v,
            None => return Err(anyhow::Error::msg("Author name can't be probed neither was it supplied"))
        },
        author_country_code: album.author_country_code,
        artwork: probe.artwork,
        tracks,
        author_id: album.author_id
    })
}

async fn check_file_type(file_path: &str) -> anyhow::Result<FileType> {
    // Probing file type
    let out = Command::new("file")
        .args(["-b", file_path])
        .output()
        .await?;
    let pat = std::str::from_utf8(&out.stdout).map(|x| x.to_lowercase())?;

    Ok(if pat.contains("audio") || pat.contains("mp4") || pat.contains("mpeg") {
        FileType::Audio
    } else if pat.contains("image") {
        FileType::Image
    } else {
        FileType::Unknown
    })
}

async fn get_audio_media_metadata(file_path: &str, probe: &mut AlbumProbe) -> anyhow::Result<Track> {
    // Fetch medatata using ffmepg
    let out = Command::new("ffmpeg")
        .args([
            "-y", "-loglevel", "error",
            "-i", file_path,
            "-f", "ffmetadata", "-"
        ])
        .output()
        .await?;

    // Some tracks may have invalid utf-8 encoded metadata so we replace it
    let out  = String::from_utf8_lossy(&out.stdout);
    let iter = out.lines();

    let mut name  = None;
    let mut track = None;
    let mut disc  = None;
    for line in iter {
        let spl = line.split_once('=');
        if let Some((k, v)) = spl {
            match k.to_lowercase().as_str() {
                "title" => {
                    name = Some(v.to_owned());
                },
                "track" => {
                    // Some metadata track have track number / total tracker number
                    let number = match v.split('/').next().and_then(|x| x.parse::<u32>().ok()) {
                        Some(v) => v,
                        None => return Err(anyhow::Error::msg("Invalid track number"))
                    };
                    track = Some(number);
                },
                "date"  => if probe.date.is_none() {
                    probe.date = Some(v.to_string());
                },
                "artist" => if probe.author.is_none() || probe.author.as_ref().unwrap().len() > v.len() {
                    probe.author = Some(v.to_string());
                },
                "album" => if probe.name.is_none() {
                    probe.name = Some(v.to_string());
                },
                "disc" => {
                    let number = match v.split('/').next().and_then(|x| x.parse::<u32>().ok()) {
                        Some(v) => v,
                        None => return Err(anyhow::Error::msg("Invalid disc number"))
                    };
                    disc = Some(number);
                }
                _ => ()
            }
        }
    }

    // Now we also need to get duration
    // codec and bitrate will be used later for transcoding
    let out = Command::new("ffprobe")
        .args([
            "-v", "error",
            "-show_entries", "stream=codec_name,duration,bit_rate,sample_rate",
            "-of", "default=noprint_wrappers=1:nokey=1",
            file_path
        ])
        .output()
        .await?;

    let mut iter = std::str::from_utf8(&out.stdout)?
        .lines();
    let codec    = match iter.next() {
        Some(v) => v,
        None => return Err(anyhow::Error::msg("Can't probe track codec"))
    };
    let sample_rate = match iter.next().and_then(|x| x.parse::<usize>().ok()) {
        Some(v) => v,
        None => return Err(anyhow::Error::msg("Can't probe track stream sample_rate"))
    };
    let duration = match iter.next().and_then(|x| x.parse::<f32>().ok()) {
        Some(v) => v,
        None => return Err(anyhow::Error::msg("Can't probe track duration"))
    };
    let bitrate = iter.next().and_then(|x| x.parse::<usize>().ok());
    let mut location = PathBuf::new();
    location.push(file_path);

    // In case we fail to probe metadata
    // we try to read track number and name from filename
    if name.is_none() || track.is_none() {
        if let Some(n) = location.file_stem().and_then(|x| x.to_str()) {
            static TRACK_NAME: OnceLock<Regex> = OnceLock::new();
            let re = TRACK_NAME.get_or_init(|| Regex::new(r"^(\d+)(\s+.\s+|\s+.\s+|\s+|.\s+|.)(.*)$").unwrap());

            if let Some(v) = re.captures(n) {
                if let (Some(t), Some(n)) = (v.get(1).and_then(|x| x.as_str().parse::<u32>().ok()), v.get(3).map(|x| x.as_str())) {
                    track = Some(t);
                    name  = Some(n.to_string());
                }
            }
        }
    }

    Ok(Track {
        name: match name {
            Some(v) => v,
            None => return Err(anyhow::Error::msg("Missing track name"))
        },
        track_number: match track {
            Some(v) => v,
            None => return Err(anyhow::Error::msg("Missing track number"))
        },
        duration: duration as i64,
        location,
        codec: codec.to_string(),
        bitrate,
        sample_rate,
        disc
    })
}
