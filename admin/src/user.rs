use anyhow::Result;
use rand::RngCore;
use wavy_shared::{permission::PermissionLevel, storage::access::AccessStorage};
use argon2::{
    password_hash::{
        rand_core::OsRng,
        PasswordHasher, SaltString
    },
    Argon2
};
use base64::Engine;

use crate::AddUser;


pub async fn create_user(access_db_path: &str, add: AddUser) -> Result<()> {
    let perm = match add.role.as_str() {
        "admin" => PermissionLevel::new_admin(),
        "normal" => PermissionLevel::default(),
        _ => return Err(anyhow::Error::msg("Invalid role for user"))
    };
    let access_db = AccessStorage::new(access_db_path).await?;

    let name = add.name.clone();
    let pass = tokio::task::spawn_blocking(move || {
        let salt   = SaltString::generate(&mut OsRng);
        let argon2 = Argon2::default();
        let pass   = argon2.hash_password(add.pass.as_bytes(), &salt)
            .unwrap()
            .to_string();

        pass
    }).await?;

    let id = access_db.add_user(&name, &pass, perm)
        .await?;
    println!("User {} with id {} added", add.name, id);

    Ok(())
}

pub async fn gen_token(access_db_path: &str, user: &uuid::Uuid) -> Result<()> {
    let access_db = AccessStorage::new(access_db_path).await?;
    let mut seed: rand::rngs::StdRng = rand::SeedableRng::from_entropy();
    let mut token                    = [0u8; 96];
    seed.fill_bytes(&mut token);

    access_db.update_user_token(user, &token)
        .await?;

    let bs64  = base64::engine::general_purpose::URL_SAFE_NO_PAD;
    let token = bs64.encode(token);

    println!("New token is {token}");

    Ok(())
}
