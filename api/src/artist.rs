use std::str::FromStr;
use serde_json::json;
use tracing::error;
use warp::reply::Response;
use wavy_shared::storage::access::UserAuth;

use crate::api::{
    artist::{GetArtist, GetArtistResp, Search, SearchType},
    resp,
    ServerState
};



pub async fn get(serv: &ServerState, _user: &UserAuth,
                 req: &GetArtist)
    -> Result<GetArtistResp, Response> {
    // Abort immediately if we get invalid page size
    if req.page_size == 0 || req.page_size > 50 {
        return Err(resp::failed(crate::PAGE_SIZE_BOUNDARIES));
    }

    let artist_info = serv.media_db
        .get_artist_info(&req.artist_id)
        .await
        .map_err(|_| resp::failed(crate::ARTIST_NOT_FOUND))?;
    
    
    let albums = serv.media_db
        .get_artist_works(&req.artist_id, &req.page, req.page_size, req.rev)
        .await
        .map_err(|e| {
            error!("Failed to fetch artist {}: {e}", req.artist_id);
            resp::service_unavailable()
        })?;
    
    Ok(GetArtistResp {
        albums,
        name: artist_info.name,
        total_pages: (artist_info.album_count + req.page_size - 1) / req.page_size
    })
}

pub async fn search(serv: &ServerState, _user: &UserAuth,
                    req: &Search)
    -> Result<serde_json::Value, Response> {
    // Abort immediately if we get invalid page size
    if req.page_size == 0 || req.page_size > 50 {
        return Err(resp::failed(crate::PAGE_SIZE_BOUNDARIES));
    }

    if req.query.trim().is_empty() || req.query.len() > 100 {
        return Err(resp::failed(crate::SEARCH_QUERY_INVALID_SIZE));
    }

    let ret = match &req.stype {
        SearchType::Song => {
            let page = req.page.split('/').collect::<Vec<&str>>();
            let album = match page.first().and_then(|x| uuid::Uuid::from_str(x).ok()) {
                Some(album) => album,
                _ => return Err(resp::failed("Invalid page parameter"))
            };
            let song = page.get(1)
                .and_then(|x| x.parse::<u32>().ok())
                .unwrap_or(0);
            serv.media_db
            .search_by_song(&req.query, &album, song, req.page_size, req.rev)
            .await
            .map(|r| json!({ "result": r }))
        },
        a => {
            let page = uuid::Uuid::from_str(&req.page)
                .map_err(|_| resp::failed("Invalid page parameter"))?;
            match a {
                SearchType::Album => serv.media_db
                    .search_by_album(&req.query, &page, req.page_size, req.rev)
                    .await
                    .map(|r| json!({ "result": r })),
                SearchType::Artist => serv.media_db
                    .search_by_artist(&req.query, &page, req.page_size, req.rev)
                    .await
                    .map(|r| json!({ "result": r })),
                _ => unreachable!()
            }
        }
    };

    let resp = ret
        .map_err(|e| {
            error!("Failed to search for query {}: {e}", req.query);
            resp::service_unavailable()
        })?;

    Ok(resp)
}
