use std::sync::Arc;

use async_stream::stream;
use futures::{Stream, StreamExt};
use tracing::error;
use warp::reply::Response;
use wavy_shared::storage::access::UserAuth;

use crate::{
    api::{
        playlist::{
            AddSongToPlaylist, AddSongToPlaylistResp, ChangeSongPosInPlaylist, ChangeSongPosInPlaylistResp, CreatePlaylist, CreatePlaylistResp, GetPlaylistSongs, GetPlaylists, GetPlaylistsResp, RemovePlaylist, RemovePlaylistResp, RemoveSongFromPlaylist, RemoveSongFromPlaylistResp
        },
        resp,
        ServerState
    },
    radio::channel::parse_song_id,
    INVALID_SONG_ID, PLAYLIST_MAX_ADD, PLAYLIST_MAX_CREATE, PLAYLIST_MAX_SONGS, PLAYLIST_NAME_TOO_LONG, SONG_ALREADY_IN_PLAYLIST, SONG_CHANGE_POS_FAILED
};


pub async fn create(serv: &ServerState, user: &UserAuth,
                 req: &CreatePlaylist)
    -> Result<CreatePlaylistResp, Response> {
    // Checking if name is too long
    if req.name.len() > 100 {
        return Err(resp::failed(PLAYLIST_NAME_TOO_LONG));
    }

    // Checking if user still can create new playlist
    match serv.access_db.user_playlists_count(&user.id).await {
        Ok(v) => if v >= user.permission.playlists_max() {
            return Err(resp::failed(PLAYLIST_MAX_CREATE));
        },
        Err(e) => {
            error!("Can't check user playlists count: {e}")
        }
    }

    // Inserting playlist
    let id = serv.access_db.add_playlist(&user.id, &req.name).await
        .map_err(|e| {
            error!("Failed created playlist: {e}");
            resp::service_unavailable()
        })?;

    Ok(CreatePlaylistResp { id })
}

pub async fn remove(serv: &ServerState, user: &UserAuth,
                 req: &RemovePlaylist)
    -> Result<RemovePlaylistResp, Response> {
    // Removing playlist
    // We only *ever* attempt to delete playlists owned by user
    serv.access_db.remove_user_playlist(&user.id, &req.id).await
        .map_err(|e| {
            error!("Failed removing user's playlist: {e}");
            resp::service_unavailable()
        })?;

    Ok(RemovePlaylistResp {})
}

pub async fn add_song_to_playlist(serv: &ServerState, user: &UserAuth,
                 req: &AddSongToPlaylist)
    -> Result<AddSongToPlaylistResp, Response> {
    // Parsing album and song id
    let (album_id, song_id) = parse_song_id(&req.song_id)?;

    // Verifying that song exists
    let exists = serv.media_db.song_exists(&album_id, song_id)
        .await
        .map_err(|e| {
            error!("Failed checking if song exists: {e}");
            resp::service_unavailable()
        })?;
    if !exists {
        return Err(resp::failed(INVALID_SONG_ID));
    }

    // Checking if limit of songs allowed in album was reached
    let count = serv.access_db.playlist_songs_count(&user.id, &req.playlist).await
        .map_err(|_| resp::service_unavailable())?;
    if count >= PLAYLIST_MAX_SONGS {
        return Err(resp::failed(PLAYLIST_MAX_ADD));
    }

    let id = serv.access_db.add_song_to_playlist(&user.id, &req.playlist, &req.song_id).await
        .map_err(|e| {
            error!("Failed adding song to playlist {e}");
            resp::service_unavailable()
        })?;

    match id {
        Some(id) => Ok(AddSongToPlaylistResp { id }),
        None => Err(resp::failed(SONG_ALREADY_IN_PLAYLIST))
    }
}

pub async fn remove_song_from_playlist(serv: &ServerState, user: &UserAuth,
                 req: &RemoveSongFromPlaylist)
    -> Result<RemoveSongFromPlaylistResp, Response> {

    serv.access_db
        .remove_song_from_playlist(&user.id, &req.playlist, req.id)
        .await
        .map_err(|e| {
            error!("Failed removing song from playlist: {e}");
            resp::service_unavailable()
        })?;

    Ok(RemoveSongFromPlaylistResp {})
}

pub async fn change_song_pos_in_playlist(serv: &ServerState, user: &UserAuth,
                                         req: &ChangeSongPosInPlaylist)
    -> Result<ChangeSongPosInPlaylistResp, Response> {

    serv.access_db
        .update_song_pos_playlist(&user.id, &req.playlist, req.pos, req.new_pos)
        .await
        .map_err(|_| resp::failed(SONG_CHANGE_POS_FAILED))?;

    Ok(ChangeSongPosInPlaylistResp {})
}

pub async fn get_playlists(serv: &ServerState, user: &UserAuth,
                           req: &GetPlaylists)
    -> Result<GetPlaylistsResp, Response> {

    let playlists = serv.access_db
        .playlists(&user.id, req.song_not_present.as_ref())
        .await
        .map_err(|e| {
            error!("Failed fetching playlists: {e}");
            resp::service_unavailable()
        })?;

    Ok(GetPlaylistsResp(playlists))
}

pub async fn get_playlist(serv: Arc<ServerState>, user: UserAuth,
                          req: GetPlaylistSongs) -> impl Stream<Item = anyhow::Result<Vec<u8>>> + Unpin {
    Box::pin(stream! {
        yield Ok(vec![b'[']);
        let mut playlist = serv.access_db
            .playlist_songs(&user.id, &req.id)
            .await
            .chunks(50);

        while let Some(v) = playlist.next().await {
            let mut ret = Vec::with_capacity(50);
            for i in v {
                ret.push(match i {
                    Ok(v) => v,
                    Err(e) => {
                        yield Err(e.into());
                        return
                    }
                });
            }

            let mut items = tokio_stream::StreamExt::peekable(serv.media_db.find_songs(ret).await);
            while let Some(Ok(v)) = items.next().await {
                match serde_json::to_vec(&v) {
                    Ok(v) => {
                        yield Ok(v);
                        if items.peek().await.is_some() {
                            yield Ok(vec![b',']);
                        }
                    },
                    Err(e) => {
                        yield Err(e.into());
                        return
                    }
                }
            }
        }
        yield Ok(vec![b']']);
    })
}

