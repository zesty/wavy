use std::path::PathBuf;
use tokio_util::io::ReaderStream;
use warp::reply::Response;
use tracing::error;

use crate::api::{artwork::GetArtwork, resp, ServerState};


pub async fn get<'a>(serv: &ServerState, req: &GetArtwork)
    -> Result<ReaderStream<tokio::fs::File>, Response> {
    let mut path = PathBuf::from(&serv.artwork_dir);
    path.push(req.album_id.to_string());
    path.push("artwork0.jpg");
    let file = tokio::fs::File::open(&path).await
        .map_err(|e| {
            error!("Failed fetching album artwork for {}: {e}", req.album_id);
            resp::not_found(crate::ARTWORK_NOT_FOUND)
        })?;

    let stream = ReaderStream::new(file);

    Ok(stream)
}
