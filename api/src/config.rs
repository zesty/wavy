use std::{
    env,
    path::{PathBuf, Path},
    net::{SocketAddr, ToSocketAddrs}
};
use anyhow::Result;


pub struct Config {
    pub node_id: String,
    pub listen_addr: String,
    pub migrate_listen_addr: String,
    pub media_db_path: String,
    pub access_db_path: String,
    pub media_dir: PathBuf,
    // Separate dir access for artwork so we can do some caching if needed
    pub artwork_dir: PathBuf,
    pub radio_server: SocketAddr,
    pub radio_source_auth: String,
    
    pub server_name: String,
    pub server_genre: String,
    pub server_desc: String
}

impl Config {
    pub fn new() -> Result<Self> {
        let node_id    = env::var("NODE_ID")?.parse()?;
        let dir_str    = env::var("LISTEN_DIR")?.parse::<String>()?;
        let listen_dir = Path::new(&dir_str);
        if !listen_dir.is_dir() {
            return Err(anyhow::Error::msg("Listen directory is not valid"));
        }
        let migrate_dir_str    = env::var("MIGRATE_LISTEN_DIR")?.parse::<String>()?;
        let migrate_listen_dir = Path::new(&migrate_dir_str);
        if !migrate_listen_dir.is_dir() {
            return Err(anyhow::Error::msg("Migrate listen directory is not valid"));
        }
        // We already knew it's valid string
        let listen_dir         = listen_dir.to_str().unwrap();
        let migrate_listen_dir = migrate_listen_dir.to_str().unwrap();
        let mut ret = Self {
            listen_addr: format!("{}{}_listen.sock", listen_dir, node_id),
            migrate_listen_addr: format!("{}{}_migrate_listen.sock", migrate_listen_dir, node_id),
            media_db_path: env::var("MEDIA_DB")?.parse()?,
            access_db_path: env::var("ACCESS_DB")?.parse()?,
            media_dir: PathBuf::new(),
            artwork_dir: PathBuf::new(),
            radio_server: env::var("RADIO_SERVER")?
                .to_socket_addrs()?
                .next()
                .ok_or(anyhow::Error::msg("Can't resolve radio server address"))?,
            radio_source_auth: env::var("RADIO_SOURCE_AUTH")?.parse()?,
            node_id,
            server_name: env::var("SERVER_NAME").unwrap_or("WavyApi".to_string()).parse()?,
            server_genre: env::var("SERVER_GENRE").unwrap_or("Misc".to_string()).parse()?,
            server_desc: env::var("SERVER_DESC").unwrap_or("Misc".to_string()).parse()?
        };

        ret.media_dir.push(env::var("MEDIA_DIR")?.parse::<String>()?);
        if !ret.media_dir.is_dir() {
            return Err(anyhow::Error::msg("Media directory is not valid"));
        }
        if let Ok(artwork_dir) = env::var("ARTWORK_DIR") {
            ret.artwork_dir.push(artwork_dir);
            if !ret.artwork_dir.is_dir() {
                return Err(anyhow::Error::msg("Artwork directory is not valid"));
            }
        } else {
            ret.artwork_dir = PathBuf::from(&ret.media_dir);
        }

        Ok(ret)
    }
}
