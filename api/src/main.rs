use anyhow::Result;
use tracing::error;

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt().with_thread_names(true).with_max_level(tracing::Level::INFO).init();

    let config = match wavy_api::config::Config::new() {
        Ok(v) => v,
        Err(e) => {
            error!("Failed to load config: {}", e);
            std::process::exit(1);
        }
    };

    wavy_api::api::run_service(config).await
}
