use std::{io::{Read, Write}, os::{fd::{FromRawFd, IntoRawFd}, unix::net::{UnixListener, UnixStream}}, sync::Arc, time::Duration};

use passfd::FdPassingExt;
// Shamelessly copy-pasted from my other project
use tokio::runtime::Handle;
use anyhow::Result;
use tracing::{error, info};
use serde::{Serialize, Deserialize};

use crate::api::ServerState;

#[derive(Serialize, Deserialize)]
struct Channel {
    id: uuid::Uuid
}

pub struct MigrateChannel {
    pub id: uuid::Uuid,
    pub conn: std::net::TcpStream
}

pub async fn spawn_listener(server: Arc<ServerState>, migrate_sock_path: String) {
    tokio::task::spawn_blocking(move || {
        if let Err(e) = migrate_predecessor(server, &migrate_sock_path) {
            error!("Migration failed: {}", e);
            std::process::exit(2);
        }
    });
}

pub async fn handle_successor(server: Arc<ServerState>, migrate_sock_path: String) {
    // For us to spawn all client tasks we must also pass runtime handle to blocking task
    let runtime_handle = Handle::current();
    _ = tokio::task::spawn_blocking(move || {
        if let Err(e) = migrate_successor(server, &migrate_sock_path, runtime_handle) {
            error!("Migration failed: {}", e);
        }
    }).await;
}

fn migrate_predecessor(server: Arc<ServerState>, migrate_sock_path: &str) -> Result<()> {
    // Start listening and wait for a successor connection
    _ = std::fs::remove_file(migrate_sock_path);
    let migrate_sock = UnixListener::bind(migrate_sock_path)
        .map_err(|_| anyhow::Error::msg("We should be able to create a unix socket"))?;

    info!("Listening on {} (Migration socket)", migrate_sock_path);
    let (mut successor, _) = migrate_sock.accept()
        .map_err(|_| anyhow::Error::msg("Can't accept connection from spawned child"))?;

    info!("New instance asking for migration, beginning migration...");

    let mut migrate  = server.migrate_tx.clone();
    let (tx, mut rx) = qanat::mpsc::unbounded_channel();
    migrate.send(tx);

    let total = server.channels
        .map
        .blocking_read()
        .len();
    let mut count = 0;
    loop {
        let ret = rx.try_recv();
        match ret {
            Ok(v) => {
                count += 1;
                match v {
                    Some(ret) => {
                        let ser = serde_json::to_vec(&Channel { id: ret.id })?;
                        successor.write_all(&(ser.len() as u64).to_be_bytes())?;
                        successor.write_all(&ser)?;
                        successor.send_fd(ret.conn.into_raw_fd())?;

                        if count >= total {
                            break;
                        }
                        std::thread::sleep(Duration::from_millis(100));
                    },
                    None => continue
                }
            },
            Err(qanat::mpsc::TryRecvError::Empty) => continue,
            Err(e) => return Err(e.into())
        };
    };

    successor.write_all(&0u64.to_be_bytes())?;

    info!("Control now passed to new instance, exiting.");

    std::process::exit(0);
}

fn migrate_successor(server: Arc<ServerState>, migrate_sock_path: &str, runtime_handle: Handle) -> Result<()> {
    let mut predeseccor = UnixStream::connect(migrate_sock_path)
        .map_err(|_| anyhow::Error::msg("No instance currently running found on migration socket"))?;

    info!("Starting migration from old instance.");

    let mut buf = vec![0u8; 4092];
    loop {
        let mut len_buf = [0u8; 8];
        predeseccor.read_exact(&mut len_buf)?;
        let len = u64::from_be_bytes(len_buf) as usize;
        if len == 0 {
            break;
        }
        predeseccor.read_exact(&mut buf[..len])?;
        let channel: Channel = serde_json::from_slice(&buf[..len])?;

        let fd = predeseccor.recv_fd()?;
        // We are sure it's valid
        let sock = unsafe { std::net::TcpStream::from_raw_fd(fd) };
        let serv = server.clone();
        runtime_handle.spawn(async move {
            serv.channels
                .new_channel(channel.id, Some(sock), serv.clone()).await;
        });
    };

    info!("Migration from old instance completed.");

    Ok(())
}
