use std::{convert::Infallible, sync::Arc};
use argon2::{Argon2, PasswordHash, PasswordVerifier};
use futures::executor::block_on;
use hashbrown::HashMap;
use qanat::{mpsc, oneshot};
use serde::Serialize;
use warp::{reject::Rejection, reply::Reply, Filter};
use base64::Engine;
use tracing::error;
use rand::RngCore;

use crate::{
    storage::access::UserAuth,
    api::ServerState
};

use super::resp;

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub enum SessionReject {
    AUTH_SESSION_EXPIRED,
    INVALID_ENCODING,
    INVALID_TOKEN,
    AUTH_MISSING
}

impl warp::reject::Reject for SessionReject {}

pub fn with_session(serv: Arc<ServerState>) -> impl Filter<Extract=(UserAuth, ), Error=Rejection> + Clone {
    let serv = warp::any().map(move || serv.clone());

    warp::header::optional::<String>("x-api-token")
        .and(warp::cookie::optional::<String>("session"))
        .and(serv)
        .and_then(move |api_token: Option<String>, cookie: Option<String>,
                        serv: Arc<ServerState>| async move {
            let bs64  = base64::engine::general_purpose::URL_SAFE_NO_PAD;
            match (cookie, api_token) {
                (Some(cookie), _) => {
                    let cookie = bs64.decode(cookie)
                        .map_err(|_| warp::reject::custom(SessionReject::INVALID_ENCODING))?;

                    serv.access_db
                        .find_user_by_cookie(&cookie)
                        .await
                        .map_err(|_| warp::reject::custom(SessionReject::AUTH_SESSION_EXPIRED))
                },
                (_, Some(api_token)) => {
                    let token = bs64.decode(api_token)
                        .map_err(|_| warp::reject::custom(SessionReject::INVALID_ENCODING))?;

                    serv.access_db
                        .find_user_by_token(&token)
                        .await
                        .map_err(|_| warp::reject::custom(SessionReject::INVALID_TOKEN))
                },
                _ => Err(warp::reject::custom(SessionReject::AUTH_MISSING))
            }
        })
}

#[derive(Debug, Serialize)]
struct AccountStatusResp {}

pub async fn status(_serv: Arc<ServerState>, _user: UserAuth)
    -> Result<warp::reply::Response, Infallible> {
    Ok(warp::reply::json(&AccountStatusResp {}).into_response())
}

pub async fn logout(serv: Arc<ServerState>, user: UserAuth)
    -> Result<warp::reply::Response, Infallible> {
    _ = serv.access_db
        .delete_user_cookie(&user.id)
        .await
        .inspect_err(|e| error!("Failed cleaning up session cookie for {}: {e}", user.id));

    Ok(warp::reply().into_response())
}

pub fn login_filter() -> impl Filter<Extract = (HashMap<String, String>,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(1024)
        .and(warp::body::form())
}

pub async fn login(serv: Arc<ServerState>, req: HashMap<String, String>)
    -> Result<warp::reply::Response, Infallible> {
    match login_inner(serv, req).await {
        Ok(v) | Err(v) => Ok(v)
    }
}

#[inline]
async fn login_inner(serv: Arc<ServerState>, mut req: HashMap<String, String>)
    -> Result<warp::reply::Response, warp::reply::Response> {
    // TODO: THIS NEEDS A CAPTCHA ASAP
    let (user, pass) = match (req.remove("user"), req.remove("pass")) {
        (Some(u), Some(p)) => (u, p),
        _ => return Err(resp::forbidden(crate::LOGIN_INVALID_CREDS))
    };

    let hash = serv.access_db
        .find_user_by_name(&user)
        .await
        .map_err(|_| resp::forbidden(crate::LOGIN_INVALID_CREDS))?;

    let (tx, mut resp) = oneshot::channel();
    serv.hasher
        .send(HashCalculation { pass, hash: hash.pass, resp: tx })
        .await
        .map_err(|e| {
            error!("Failed sending hashing request: {}", e.0);
            resp::forbidden(crate::LOGIN_INVALID_CREDS)
        })?;

    let cookie = resp.recv()
        .await
        .map_err(|e| {
            error!("Failed receiving hashing response: {e}");
            resp::forbidden(crate::LOGIN_INVALID_CREDS)
        })?;

    match cookie {
        Some(cookie) => {
            serv.access_db
                .update_user_cookie(&hash.id, &cookie)
                .await
                .map_err(|_| resp::forbidden(crate::LOGIN_INVALID_CREDS))?;

            let bs64 = base64::engine::general_purpose::URL_SAFE_NO_PAD;
            Ok(warp::reply::with_header(
                warp::reply(),
                "set-cookie",
                format!("session={}", bs64.encode(&cookie))
            ).into_response())
        },
        None => Err(resp::forbidden(crate::LOGIN_INVALID_CREDS))
    }
}

pub struct HashCalculation {
    pub pass: String,
    pub hash: String,
    pub resp: oneshot::Sender<Option<Vec<u8>>>
}

pub async fn login_hasher(mut rx: mpsc::Receiver<HashCalculation>) {
    tokio::task::spawn_blocking(move || {
        let argon2 = Argon2::default();

        while let Ok(req) = block_on(rx.recv()) {
            let parsed_hash = match PasswordHash::new(&req.hash) {
                Ok(v) => v,
                Err(e) => {
                    error!("Invalid hash: {e}");
                    _ = req.resp.send(None);
                    continue;
                }
            };
            let resp = match argon2.verify_password(req.pass.as_bytes(), &parsed_hash) {
                Ok(_) => {
                    let mut seed: rand::rngs::StdRng = rand::SeedableRng::from_entropy();
                    let mut cookie                   = vec![0u8; 64];
                    seed.fill_bytes(&mut cookie);
                    Some(cookie)
                },
                Err(_) => None
            };
            _ = req.resp.send(resp);
        }
    });
}
