use std::{convert::Infallible, sync::Arc};
use anyhow::Result;
use serde::Deserialize;
use warp::Filter;

use super::ServerState;


#[derive(Debug, Deserialize)]
pub struct GetArtwork {
    pub album_id: uuid::Uuid,
}

pub fn get_filter() -> impl Filter<Extract = (GetArtwork,), Error = warp::Rejection> + Clone {
    // Here we are ignoring if song_id was also sent to us
    warp::path::param()
        .map(|album_id: uuid::Uuid| {
            GetArtwork { album_id }
        })
}

pub async fn get(serv: Arc<ServerState>, req: GetArtwork)
    -> Result<warp::reply::Response, Infallible> {
    let stream = match crate::artwork::get(&serv, &req).await {
        Ok(v) => v,
        Err(e) => return Ok(e)
    };

    let mut resp = warp::reply::Response::new(warp::hyper::Body::wrap_stream(stream));
    resp.headers_mut()
        .insert("Cache-Control", warp::http::HeaderValue::from_static("public, max-age=604800, immutable"));

    Ok(resp)
}
