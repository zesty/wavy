use std::{convert::Infallible, sync::Arc};
use sqlx::types::Uuid;
use warp::{Filter, reply::Reply};
use serde::{Deserialize, Serialize};
use wavy_shared::storage::access::UserAuth;

use crate::api::ServerState;

#[derive(Debug, Deserialize)]
pub struct AddChannel {
    pub name: String,
    pub description: String,
}

#[derive(Debug, Serialize)]
pub struct AddChannelResp {
    pub id: Uuid
}

pub fn post_create_channel_filter() -> impl Filter<Extract = (AddChannel,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(4096)
        .and(warp::body::json())
}

pub async fn create_channel(serv: Arc<ServerState>, user: UserAuth, add: AddChannel)
    -> Result<warp::reply::Response, Infallible> {
    let resp = match crate::radio::channel::create_channel(serv, &user, add).await {
        Ok(v) => v,
        Err(e) => return Ok(e)
    };

    Ok(warp::reply::json(&resp).into_response())
}

pub fn post_remove_channel_filter() -> impl Filter<Extract = (RemoveChannel,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(4096)
        .and(warp::body::json())
}

#[derive(Debug, Deserialize)]
pub struct RemoveChannel {
    pub id: Uuid
}

#[derive(Debug, Serialize)]
pub struct RemoveChannelResp {}

pub async fn remove_channel(serv: Arc<ServerState>, user: UserAuth, remove: RemoveChannel)
    -> Result<warp::reply::Response, Infallible> {
    let resp = match crate::radio::channel::remove_channel(&serv, &user, &remove).await {
        Ok(v) => v,
        Err(e) => return Ok(e)
    };

    Ok(warp::reply::json(&resp).into_response())
}

pub fn song_request_filter() -> impl Filter<Extract = (SongRequest,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(1024)
        .and(warp::body::json())
}

#[derive(Debug, Deserialize)]
pub struct SongRequest {
    pub channel_id: Uuid,
    pub song_id: String
}

#[derive(Debug, Serialize)]
pub struct SongRequestResp {}

pub async fn song_request(serv: Arc<ServerState>, user: UserAuth, remove: SongRequest)
    -> Result<warp::reply::Response, Infallible> {
    let resp = match crate::radio::channel::song_request(&serv, &user, &remove).await {
        Ok(v) => v,
        Err(e) => return Ok(e)
    };

    Ok(warp::reply::json(&resp).into_response())
}

pub fn channel_events_filter() -> impl Filter<Extract = (EventsRequest,), Error = warp::Rejection> + Clone {
    warp::path::param()
        .map(|channel_id: uuid::Uuid| {
            EventsRequest { channel_id }
        })
}

#[derive(Debug, Deserialize)]
pub struct EventsRequest {
    pub channel_id: Uuid
}

pub async fn channel_events(serv: Arc<ServerState>, req: EventsRequest)
    -> Result<warp::reply::Response, Infallible> {
    let stream = match crate::radio::channel::events(&serv, &req).await {
        Ok(v) => v,
        Err(e) => return Ok(e)
    };

    Ok(warp::sse::reply(warp::sse::keep_alive().stream(stream)).into_response())
}
