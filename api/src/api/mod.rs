pub mod radio;
mod account;
pub mod resp;
pub mod artist;
pub mod artwork;
pub mod playlist;

use std::{sync::Arc, convert::Infallible, path::PathBuf, net::SocketAddr, num::NonZeroUsize, os::unix::fs::PermissionsExt};
use anyhow::Result;
use account::HashCalculation;
use tracing::error;
use crate::{
    radio::{channel::Channels, playlist::ChannelsPlaylist},
    config::Config, migrate::MigrateChannel,
    storage::{access::AccessStorage, media::MediaInfoStorage}
};
use tokio::net::UnixListener;
use tokio_stream::wrappers::UnixListenerStream;
use warp::{
    Filter, reject::Rejection,
    reply::{Response, Reply}, http::StatusCode
};
use qanat::{broadcast, mpsc};

pub struct ServerState {
    pub node_id: String,
    pub media_db: MediaInfoStorage,
    pub access_db: AccessStorage,
    pub channels: Arc<Channels>,
    pub playlist: Arc<ChannelsPlaylist>,
    pub media_dir: PathBuf,
    pub artwork_dir: PathBuf,
    pub radio_server: SocketAddr,
    pub radio_source_auth: String,
    pub hasher: mpsc::Sender<HashCalculation>,
    pub migrate: broadcast::Receiver<mpsc::UnboundedSender<Option<MigrateChannel>>>,
    pub migrate_tx: broadcast::Sender<mpsc::UnboundedSender<Option<MigrateChannel>>>,
    pub server_properties: Properties
}

pub struct Properties {
    pub name: String,
    pub genre: String,
    pub desc: String
}

pub async fn run_service(config: Config) -> Result<()> {
    let (tx, rx)   = broadcast::channel(NonZeroUsize::MIN);
    let (tx1, rx1) = mpsc::channel(40.try_into().expect("40 > 0 should work"));
    let serv = Arc::new(ServerState {
        node_id: config.node_id,
        media_db: MediaInfoStorage::new(&config.media_db_path).await?,
        access_db: AccessStorage::new(&config.access_db_path).await?,
        channels: Arc::new(Channels::default()),
        playlist: Arc::new(ChannelsPlaylist::default()),
        media_dir: config.media_dir,
        artwork_dir: config.artwork_dir,
        radio_server: config.radio_server,
        radio_source_auth: config.radio_source_auth,
        hasher: tx1,
        migrate: rx,
        migrate_tx: tx,
        server_properties: Properties {
            name: config.server_name.clone(),
            genre: config.server_genre,
            desc: config.server_desc
        }
    });

    let server      = serv.clone();
    let serv        = warp::any().map(move || serv.clone() );
    let incoming    = setup_socket(&config.listen_addr)
        .await
        .map_err(|e| anyhow::Error::msg(format!("Failed to start listener: {}", e)))?;

    account::login_hasher(rx1).await;
    crate::migrate::handle_successor(server.clone(), config.migrate_listen_addr.clone()).await;
    crate::migrate::spawn_listener(server.clone(), config.migrate_listen_addr).await;
    crate::radio::channel::spawn_channel_tasks(server.clone()).await;

    let new_channel = warp::post()
        .and(warp::path("v1"))
        .and(warp::path("newchannel"))
        .and(serv.clone())
        .and(account::with_session(server.clone()))
        .and(radio::post_create_channel_filter())
        .and_then(radio::create_channel);

    let remove_channel = warp::post()
        .and(warp::path("v1"))
        .and(warp::path("removechannel"))
        .and(serv.clone())
        .and(account::with_session(server.clone()))
        .and(radio::post_remove_channel_filter())
        .and_then(radio::remove_channel);

    let song_request = warp::post()
        .and(warp::path("v1"))
        .and(warp::path("songrequest"))
        .and(serv.clone())
        .and(account::with_session(server.clone()))
        .and(radio::song_request_filter())
        .and_then(radio::song_request);

    let channel_events = warp::get()
        .and(warp::path("v1"))
        .and(warp::path("channelevents"))
        .and(serv.clone())
        .and(radio::channel_events_filter())
        .and_then(radio::channel_events);

    let song_artwork = warp::get()
        .and(warp::path("v1"))
        .and(warp::path("songartwork"))
        .and(serv.clone())
        .and(artwork::get_filter())
        .and_then(artwork::get)
        .map(move |x| {
            warp::reply::with_header(x, "Content-Type", "image/jpeg")
        });

    let artist = warp::post()
        .and(warp::path("v1"))
        .and(warp::path("artist"))
        .and(serv.clone())
        .and(account::with_session(server.clone()))
        .and(artist::get_filter())
        .and_then(artist::get);

    let search = warp::post()
        .and(warp::path("v1"))
        .and(warp::path("search"))
        .and(serv.clone())
        .and(account::with_session(server.clone()))
        .and(artist::search_filter())
        .and_then(artist::search);

    let login = warp::post()
        .and(warp::path("v1"))
        .and(warp::path("login"))
        .and(serv.clone())
        .and(account::login_filter())
        .and_then(account::login);

    let logout = warp::get()
        .and(warp::path("v1"))
        .and(warp::path("logout"))
        .and(serv.clone())
        .and(account::with_session(server.clone()))
        .and_then(account::logout);

    let status = warp::get()
        .and(warp::path("v1"))
        .and(warp::path("status"))
        .and(serv.clone())
        .and(account::with_session(server.clone()))
        .and_then(account::status);

    let new_playlist = warp::post()
        .and(warp::path("v1"))
        .and(warp::path("playlist"))
        .and(warp::path("new"))
        .and(serv.clone())
        .and(account::with_session(server.clone()))
        .and(playlist::create_filter())
        .and_then(playlist::create);

    let remove_playlist = warp::post()
        .and(warp::path("v1"))
        .and(warp::path("playlist"))
        .and(warp::path("remove"))
        .and(serv.clone())
        .and(account::with_session(server.clone()))
        .and(playlist::remove_filter())
        .and_then(playlist::remove);

    let add_to_playlist = warp::post()
        .and(warp::path("v1"))
        .and(warp::path("playlist"))
        .and(warp::path("addsong"))
        .and(serv.clone())
        .and(account::with_session(server.clone()))
        .and(playlist::add_song_to_playlist_filter())
        .and_then(playlist::add_song_to_playlist);
    
    let remove_from_playlist = warp::post()
        .and(warp::path("v1"))
        .and(warp::path("playlist"))
        .and(warp::path("removesong"))
        .and(serv.clone())
        .and(account::with_session(server.clone()))
        .and(playlist::remove_song_from_playlist_filter())
        .and_then(playlist::remove_song_from_playlist);

    let change_pos = warp::post()
        .and(warp::path("v1"))
        .and(warp::path("playlist"))
        .and(warp::path("changesongpos"))
        .and(serv.clone())
        .and(account::with_session(server.clone()))
        .and(playlist::change_song_pos_in_playlist_filter())
        .and_then(playlist::change_song_pos_in_playlist);

    let playlist_songs = warp::post()
        .and(warp::path("v1"))
        .and(warp::path("playlist"))
        .and(warp::path("songs"))
        .and(serv.clone())
        .and(account::with_session(server.clone()))
        .and(playlist::get_playlist_filter())
        .and_then(playlist::get_playlist);

    let get_playlists = warp::post()
        .and(warp::path("v1"))
        .and(warp::path("playlists"))
        .and(serv.clone())
        .and(account::with_session(server.clone()))
        .and(playlist::get_playlists_filter())
        .and_then(playlist::get_playlists);


    let routes = new_channel
        .or(remove_channel)
        .or(song_request)
        .or(channel_events)
        .or(song_artwork)
        .or(artist)
        .or(search)
        .or(login)
        .or(logout)
        .or(status)
        .or(new_playlist)
        .or(remove_playlist)
        .or(add_to_playlist)
        .or(remove_from_playlist)
        .or(change_pos)
        .or(playlist_songs)
        .or(get_playlists)
        // In case there is any error (path not found, invalid request, ... etc)
        // we want to send an empty body or a json response back
        .recover(reject)
        .map(move |x| {
            warp::reply::with_header(x, "server", &config.server_name)
        });

    warp::serve(routes)
        .run_incoming(incoming)
        .await;

    Ok(())
}

async fn reject(err: Rejection) -> Result<Response, Infallible> {
    let error;

    if err.is_not_found() {
        return Ok(warp::reply::with_status(
            "",
            StatusCode::NOT_FOUND
        ).into_response());
    } else if let Some(e) = err.find::<warp::filters::body::BodyDeserializeError>() {
        error = (
            e.to_string(),
            StatusCode::BAD_REQUEST
        );
    } else if let Some(e) = err.find::<warp::reject::PayloadTooLarge>() {
        error = (
            e.to_string(),
            StatusCode::PAYLOAD_TOO_LARGE
        );
    } else if let Some(e) = err.find::<account::SessionReject>() {
        return Ok(match e {
            account::SessionReject::AUTH_MISSING => warp::reply::with_status(
                warp::reply::json(&resp::Error { error: crate::AUTH_MISSING }),
                StatusCode::UNAUTHORIZED
            ).into_response(),
            account::SessionReject::AUTH_SESSION_EXPIRED => resp::forbidden(crate::AUTH_SESSION_EXPIRED),
            account::SessionReject::INVALID_ENCODING => resp::forbidden(crate::INVALID_ENCODING),
            account::SessionReject::INVALID_TOKEN => resp::forbidden(crate::INVALID_TOKEN)
        });
    } else if err.find::<warp::reject::MethodNotAllowed>().is_some() {
        return Ok(warp::reply::with_status(
            "",
            StatusCode::METHOD_NOT_ALLOWED
        ).into_response());
    } else {
        error!("Unhandled rejection: {:?}", err);
        return Ok(resp::service_unavailable());
    }

    Ok(warp::reply::with_status(
        warp::reply::json(&resp::Error { error: &error.0 }),
        error.1
    ).into_response())
}

async fn setup_socket(listen_addr: &str) -> Result<UnixListenerStream> {
    _            = tokio::fs::remove_file(listen_addr).await;
    let listener = UnixListener::bind(listen_addr)?;
    // Let any user access this unix socket
    let meta     = tokio::fs::metadata(listen_addr).await?;
    let mut perm = meta.permissions();
    perm.set_mode(0o777);
    _            = tokio::fs::set_permissions(listen_addr, perm).await;
    let incoming = UnixListenerStream::new(listener);

    Ok(incoming)
}
