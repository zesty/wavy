use std::{convert::Infallible, sync::Arc};
use anyhow::Result;
use serde::{Deserialize, Serialize};
use warp::{reply::Reply, Filter};
use wavy_shared::storage::{access::UserAuth, media::AlbumDisplay};

use super::ServerState;

fn default_page() -> uuid::Uuid { uuid::Uuid::nil() }
fn default_page_str() -> String { default_page().to_string() }
fn default_page_size() -> u32 { 20 }
fn default_reverse_page_lookup() -> bool { false }

#[derive(Debug, Deserialize)]
pub struct GetArtist {
    pub artist_id: uuid::Uuid,
    #[serde(default = "default_page")]
    pub page: uuid::Uuid,
    #[serde(default = "default_page_size")]
    pub page_size: u32,
    #[serde(default = "default_reverse_page_lookup")]
    pub rev: bool
}

#[derive(Debug, Serialize)]
pub struct GetArtistResp {
    pub name: String,
    pub albums: Vec<AlbumDisplay>,
    pub total_pages: u32
}

pub fn get_filter() -> impl Filter<Extract = (GetArtist,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(1024)
        .and(warp::body::json())
}

pub async fn get(serv: Arc<ServerState>, user: UserAuth, req: GetArtist)
    -> Result<warp::reply::Response, Infallible> {
    let resp = match crate::artist::get(&serv, &user, &req).await {
        Ok(v) => v,
        Err(e) => return Ok(e)
    };

    Ok(warp::reply::json(&resp).into_response())
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")] 
pub enum SearchType {
    Artist,
    Album,
    Song
}

#[derive(Debug, Deserialize)]
pub struct Search {
    pub query: String,
    #[serde(rename = "type")]
    pub stype: SearchType,
    #[serde(default = "default_page_str")]
    pub page: String,
    #[serde(default = "default_page_size")]
    pub page_size: u32,
    #[serde(default = "default_reverse_page_lookup")]
    pub rev: bool
}

pub fn search_filter() -> impl Filter<Extract = (Search,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(1024)
        .and(warp::body::json())
}

pub async fn search(serv: Arc<ServerState>, user: UserAuth, req: Search)
    -> Result<warp::reply::Response, Infallible> {
    let resp = match crate::artist::search(&serv, &user, &req).await {
        Ok(v) => v,
        Err(e) => return Ok(e)
    };

    Ok(warp::reply::json(&resp).into_response())
}
