use std::{convert::Infallible, sync::Arc};
use anyhow::Result;
use warp::{reply::Reply, Filter};
use serde::{Deserialize, Serialize};

use wavy_shared::storage::access::{Playlist, UserAuth};

use super::ServerState;

#[derive(Debug, Deserialize)]
pub struct CreatePlaylist {
    pub name: String
}

#[derive(Debug, Serialize)]
pub struct CreatePlaylistResp {
    pub id: uuid::Uuid
}

pub fn create_filter() -> impl Filter<Extract = (CreatePlaylist,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(512)
        .and(warp::body::json())
}

pub async fn create(serv: Arc<ServerState>, user: UserAuth, req: CreatePlaylist)
    -> Result<warp::reply::Response, Infallible> {
    let resp = match crate::playlist::create(&serv, &user, &req).await {
        Ok(v) => v,
        Err(e) => return Ok(e)
    };

    Ok(warp::reply::json(&resp).into_response())
}

#[derive(Debug, Deserialize)]
pub struct RemovePlaylist {
    pub id: uuid::Uuid
}

#[derive(Debug, Serialize)]
pub struct RemovePlaylistResp {}

pub fn remove_filter() -> impl Filter<Extract = (RemovePlaylist,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(512)
        .and(warp::body::json())
}

pub async fn remove(serv: Arc<ServerState>, user: UserAuth, req: RemovePlaylist)
    -> Result<warp::reply::Response, Infallible> {
    let resp = match crate::playlist::remove(&serv, &user, &req).await {
        Ok(v) => v,
        Err(e) => return Ok(e)
    };

    Ok(warp::reply::json(&resp).into_response())
}

#[derive(Debug, Deserialize)]
pub struct AddSongToPlaylist {
    pub playlist: uuid::Uuid,
    pub song_id: String
}

#[derive(Debug, Serialize)]
pub struct AddSongToPlaylistResp {
    pub id: u32
}

pub fn add_song_to_playlist_filter() -> impl Filter<Extract = (AddSongToPlaylist,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(512)
        .and(warp::body::json())
}

pub async fn add_song_to_playlist(serv: Arc<ServerState>, user: UserAuth, req: AddSongToPlaylist)
    -> Result<warp::reply::Response, Infallible> {
    let resp = match crate::playlist::add_song_to_playlist(&serv, &user, &req).await {
        Ok(v) => v,
        Err(e) => return Ok(e)
    };

    Ok(warp::reply::json(&resp).into_response())
}

#[derive(Debug, Deserialize)]
pub struct RemoveSongFromPlaylist {
    pub playlist: uuid::Uuid,
    pub id: u32
}

#[derive(Debug, Serialize)]
pub struct RemoveSongFromPlaylistResp {}

pub fn remove_song_from_playlist_filter() -> impl Filter<Extract = (RemoveSongFromPlaylist,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(512)
        .and(warp::body::json())
}

pub async fn remove_song_from_playlist(serv: Arc<ServerState>, user: UserAuth, req: RemoveSongFromPlaylist)
    -> Result<warp::reply::Response, Infallible> {
    let resp = match crate::playlist::remove_song_from_playlist(&serv, &user, &req).await {
        Ok(v) => v,
        Err(e) => return Ok(e)
    };

    Ok(warp::reply::json(&resp).into_response())
}

#[derive(Debug, Deserialize)]
pub struct ChangeSongPosInPlaylist {
    pub playlist: uuid::Uuid,
    pub pos: u32,
    pub new_pos: u32
}

#[derive(Debug, Serialize)]
pub struct ChangeSongPosInPlaylistResp {}

pub fn change_song_pos_in_playlist_filter() -> impl Filter<Extract = (ChangeSongPosInPlaylist,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(512)
        .and(warp::body::json())
}

pub async fn change_song_pos_in_playlist(serv: Arc<ServerState>, user: UserAuth, req: ChangeSongPosInPlaylist)
    -> Result<warp::reply::Response, Infallible> {
    let resp = match crate::playlist::change_song_pos_in_playlist(&serv, &user, &req).await {
        Ok(v) => v,
        Err(e) => return Ok(e)
    };

    Ok(warp::reply::json(&resp).into_response())
}

#[derive(Debug, Deserialize)]
pub struct GetPlaylists {
    pub song_not_present: Option<String>
}

#[derive(Debug, Serialize)]
pub struct GetPlaylistsResp(pub Vec<Playlist>);

pub fn get_playlists_filter() -> impl Filter<Extract = (GetPlaylists,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(512)
        .and(warp::body::json())
}

pub async fn get_playlists(serv: Arc<ServerState>, user: UserAuth, req: GetPlaylists)
    -> Result<warp::reply::Response, Infallible> {
    let resp = match crate::playlist::get_playlists(&serv, &user, &req).await {
        Ok(v) => v,
        Err(e) => return Ok(e)
    };

    Ok(warp::reply::json(&resp).into_response())
}

#[derive(Debug, Deserialize)]
pub struct GetPlaylistSongs {
    pub id: uuid::Uuid
}

pub fn get_playlist_filter() -> impl Filter<Extract = (GetPlaylistSongs,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(512)
        .and(warp::body::json())
}

pub async fn get_playlist(serv: Arc<ServerState>, user: UserAuth, req: GetPlaylistSongs)
    -> Result<warp::reply::Response, Infallible> {
    let stream   = crate::playlist::get_playlist(serv, user, req).await;
    let mut resp = warp::reply::Response::new(warp::hyper::Body::wrap_stream(stream));
    resp.headers_mut()
        .insert("Content-Type", warp::http::HeaderValue::from_static("application/json"));

    Ok(resp)
}
