use serde::Serialize;
use warp::{http::StatusCode, reply::Reply};

#[derive(Serialize)]
pub struct Error<'a> {
    pub error: &'a str,
}

pub fn service_unavailable() -> warp::reply::Response {
    warp::reply::with_status(
        "",
        StatusCode::SERVICE_UNAVAILABLE
    ).into_response()
}

pub fn forbidden(msg: &str) -> warp::reply::Response {
    warp::reply::with_status(
        warp::reply::json(&Error { error: msg }),
        StatusCode::FORBIDDEN
    ).into_response()
}

pub fn bad_request(msg: &str) -> warp::reply::Response {
    warp::reply::with_status(
        warp::reply::json(&Error { error: msg }),
        StatusCode::BAD_REQUEST
    ).into_response()
}

pub fn not_found(msg: &str) -> warp::reply::Response {
    warp::reply::with_status(
        warp::reply::json(&Error { error: msg }),
        StatusCode::NOT_FOUND
    ).into_response()
}

pub fn failed(msg: &str) -> warp::reply::Response {
    warp::reply::json(&Error { error: msg }).into_response()
}
