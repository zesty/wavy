pub mod api;
pub mod config;
pub mod migrate;
pub mod artist;
pub mod artwork;
pub mod radio;
pub mod playlist;
pub use wavy_shared::*;

pub const MAX_CONCURRENT_REQUESTS: usize   = 2000;
pub const MAX_CHANNELS_NUMBER: usize       = 20;
/// When channel reachs an error state (can't reach radio server, fetch song from fs)
/// Timeout in ms
pub const CHANNEL_ERROR_TIMEOUT: u64       = 2000;
pub const CHANNEL_REQUEST_POLL_INT: u64    = 3000;
/// Interval to save channel playback progress in seconds
pub const CHANNEL_UPDATE_PROGRESS_INT: u64 = 5;
/// MAX songs allowed in a given playlist
pub const PLAYLIST_MAX_SONGS: u32          = 1000;



// Error messages returned to user
pub const AUTH_SESSION_EXPIRED: &str       = "Session expired";
pub const INVALID_ENCODING: &str           = "Invalid encodage for cookie/token";
pub const INVALID_TOKEN: &str              = "Invalid token";
pub const AUTH_MISSING: &str               = "User not authenticated";

pub const CHANNEL_CREATION_DENIED: &str    = "Permission denied or max number of channels reached";
pub const USER_DONT_OWN_CHANNEL: &str      = "You don't own channel to delete it";
pub const INVALID_SONG_ID: &str            = "Invalid song id";
pub const INVALID_CHANNEL_ID: &str         = "Channel doesn't exist with given id";
pub const CHANNEL_QUEUE_LIMIT: &str        = "Channel reached max request capacity for now";

pub const ARTWORK_NOT_FOUND: &str          = "Artwork not found";

pub const ARTIST_NOT_FOUND: &str           = "Artist not found";
pub const PAGE_OUT_OF_RANGE: &str          = "Requested page index is out of range";
pub const PAGE_SIZE_BOUNDARIES: &str       = "Page size must be bigger than 0 and smaller than 50";

pub const SEARCH_QUERY_INVALID_SIZE: &str  = "Search query size is either 0 or too big to handle";

pub const LOGIN_INVALID_CREDS: &str        = "Username or password is invalid";

pub const PLAYLIST_NAME_TOO_LONG: &str     = "Playlist name is too long, try using a shorter one";
pub const PLAYLIST_MAX_CREATE: &str        = "Playlist cannot be created because you have reached max number of playlists allowed for you";
pub const PLAYLIST_MAX_ADD: &str           = "Song cannot be added to playlist because limit was reached";
pub const SONG_ALREADY_IN_PLAYLIST: &str   = "Song is already in playlist";
pub const SONG_CHANGE_POS_FAILED: &str     = "Failed changing song position in playlist";
