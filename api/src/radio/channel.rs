use std::{convert::Infallible, path::PathBuf, str::FromStr, sync::Arc, time::Duration};
use hashbrown::HashMap;
use anyhow::Result;
use tokio::{io::AsyncReadExt, sync::{mpsc, RwLock}};
use tokio_stream::{Stream, StreamExt};
use tracing::error;
use warp::reply::Response;
use wavy_shared::storage::media::SongRequestPicker;

use crate::{
    api::{
        radio::{AddChannel, AddChannelResp, EventsRequest, RemoveChannel, RemoveChannelResp, SongRequest, SongRequestResp},
        resp, ServerState
    },
    migrate::MigrateChannel,
    radio::{
        player::{PlayNotification, Player, SongResume},
        playlist::{ChannelPlaylistWriter, PlaylistUpdate, SongInfo},
    },
    storage::access::UserAuth
};

pub enum ChannelEvent {
    AddSongToQueue {
        request_id: i64
    },
    RemoveChannel
}

#[derive(Debug)]
pub struct Channels {
    pub map: RwLock<HashMap<uuid::Uuid, Channel>>
}

impl Default for Channels {
    fn default() -> Self {
        Self {
            map: RwLock::new(HashMap::new())
        }
    }
}

impl Channels {
    pub async fn new_channel(&self, ch_id: uuid::Uuid, sock: Option<std::net::TcpStream>, serv: Arc<ServerState>) {
        let (tx, rx) = mpsc::unbounded_channel();

        let playlist = serv.playlist
            .add(ch_id)
            .await;
        let ch_id_clone = ch_id;
        tokio::spawn(async move {
            if let Err(e) = channel_player(ch_id_clone, rx, sock, serv, playlist).await {
                error!("{}", e);
            }
        });
        self.map.write()
            .await
            .insert(ch_id, Channel { queue: tx });
    }

    pub async fn remove_channel(&self, ch_id: &uuid::Uuid, serv: &ServerState) {
        serv.playlist
            .remove(ch_id)
            .await;
        let ch = self.map.write()
            .await
            .remove(ch_id);

        if let Some(ch) = ch {
            _ = ch.queue.send(ChannelEvent::RemoveChannel);
        }
    }
}

pub async fn spawn_channel_tasks(serv: Arc<ServerState>) {
    {
        let mut stream = serv.access_db
            .channels(&serv.node_id)
            .await;

        while let Some(channel) = stream.next().await {
            match channel {
                Ok(channel) => {
                    if serv.channels
                        .map
                        .read()
                        .await
                        .contains_key(&channel.id) {
                        // It was already added in migration
                        continue;
                    }
                    serv.channels.new_channel(channel.id, None, serv.clone()).await
                },
                Err(e) => error!("Error fetching channel to spawn it's task: {}", e)
            }
        }
    }

    tokio::spawn(async move {
        let mut last_id = -1;
        loop {
            let chs = serv.access_db
                .channels_last_request(&serv.node_id, last_id)
                .await
                // We don't care because ultimately exit before this throws an error (due to new scheme
                // version... etc), hopefully...
                .inspect_err(|e| error!("Channel request poller can't fetch updates: {}", e))
                .ok()
                .unwrap_or(Vec::new());

            for ch in &chs {
                let tx = serv.channels
                    .map
                    .read()
                    .await
                    .get(&ch.id)
                    .map(|ch| ch.queue.clone());
                if let Some(tx) = tx {
                    _ = tx.send(ChannelEvent::AddSongToQueue { request_id: ch.request_id });
                }
                if ch.request_id > last_id {
                    last_id = ch.request_id;
                }
            }
            tokio::time::sleep(Duration::from_millis(crate::CHANNEL_REQUEST_POLL_INT)).await;
        };
    });
}

struct ChannelState {
    id: uuid::Uuid,
    serv: Arc<ServerState>,
    last_request_id: i64,
    rx: mpsc::UnboundedReceiver<ChannelEvent>,
    player: Player,
    playlist: Option<ChannelPlaylistWriter>
}

impl std::fmt::Display for ChannelState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "channel:[id:{}]", self.id)
    }
}

async fn channel_player(id: uuid::Uuid,
                        rx: mpsc::UnboundedReceiver<ChannelEvent>,
                        sock: Option<std::net::TcpStream>,
                        serv: Arc<ServerState>,
                        playlist: Option<ChannelPlaylistWriter>) -> Result<()> {
    if let Err(e) = add_automated_request(&id, &serv).await {
        return Err(anyhow::Error::msg(format!("Can't fetch any song from db: {}", e)));
    }


    let ch_mount = format!("/{}.mp3", id);
    let player;
    if let Some(sock) = sock {
        player = Player::from_connection(sock, &ch_mount);
    } else {
        loop {
            if let Ok(v) = Player::new(serv.clone(), &ch_mount).await {
                player = v;
                break;
            }
            tokio::time::sleep(Duration::from_millis(crate::CHANNEL_ERROR_TIMEOUT)).await;
        };
    }
    let mut state = ChannelState {
        id,
        serv,
        last_request_id: -2,
        rx,
        player,
        playlist
    };

    // Fetching all song requests for channel
    if add_new_request_to_queue(&mut state, -1).await.is_err() {
        return Err(anyhow::Error::msg(format!("{} stopped due to db communication error", state)));
    }

    // First ever metadata
    if let Err(e) = state.player.update_stream_metadata(&state.serv).await {
        error!("{} can't update metadata: {}", state, e);
    }

    loop {
        match channel_event_handler(&mut state).await {
            ChannelEventHandlerState::Exit => return Ok(()),
            ChannelEventHandlerState::Unhandled => 
                return Err(anyhow::Error::msg(format!("{} exiting due to an unrecoverable error", state))),
            ChannelEventHandlerState::ServerError => {
                loop {
                    tokio::time::sleep(Duration::from_millis(crate::CHANNEL_ERROR_TIMEOUT)).await;
                    match Player::new(state.serv.clone(), &ch_mount).await {
                        Ok(v) => state.player = v,
                        Err(e) => {
                            error!("{} failed to reach radio server: {}", state, e);
                            continue;
                        }
                    }
                    break;
                }
            },
            ChannelEventHandlerState::StorageError { conn } => {
                state.player = Player::from_connection(conn, &ch_mount);
                tokio::time::sleep(Duration::from_millis(crate::CHANNEL_ERROR_TIMEOUT)).await;
            }
        }
        // We must refetch requests in queue because we initiated a new player
        if add_new_request_to_queue(&mut state, -1).await.is_err() {
            return Err(anyhow::Error::msg(format!("{} stopped due to db communication error", state)));
        }
    };
}

enum ChannelEventHandlerState {
    /// Either we are removing channel, or something terribly wrong happened ¯_(ツ)_/¯
    Unhandled,
    StorageError { conn: std::net::TcpStream },
    ServerError,
    /// Channel got removed or migrated
    Exit
}

async fn channel_event_handler(state: &mut ChannelState) -> ChannelEventHandlerState {
    let mut mig = state.serv.migrate.clone();
    let mut migrate: Option<qanat::mpsc::UnboundedSender<Option<MigrateChannel>>> = None;
    loop {
        tokio::select! {
            r = state.player.playback_notification() => match r {
                Ok(v) => match v {
                    PlayNotification::SongFinished { request_id } => {
                        if let Err(e) = state.serv
                            .access_db
                            .remove_finished_request(request_id)
                            .await {
                            error!("{}: Can't save finished song request state to db: {}", state, e);
                            // TODO: Can we handle this better
                            return ChannelEventHandlerState::Unhandled;
                        }
                        // Updating metadata for new song
                        if let Err(e) = state.player.update_stream_metadata(&state.serv).await {
                            error!("{} can't update metadata: {}", state, e);
                        }
                        // Notify clients that song finished
                        change_playlist_currenctly_playing(state).await;
                        // Add random request in case we are running out of user requests
                        if let Err(e) = add_automated_request(&state.id, &state.serv).await {
                            error!("{}: Failed adding automated request: {}", state, e);
                            return ChannelEventHandlerState::Unhandled;
                        }
                        // We are sure there is new requests in queue
                        if let Err(e) = add_new_request_to_queue(state, i64::MAX).await { return e };
                    },
                    PlayNotification::ServerError(resume) => {
                        if let Err(e) = save_progress(state, resume).await { return e };
                        return ChannelEventHandlerState::ServerError;
                    },
                    PlayNotification::StorageError { resume, conn } => {
                        if let Err(e) = save_progress(state, resume).await { return e };
                        return ChannelEventHandlerState::StorageError { conn };
                    },
                    PlayNotification::Stop(resume) => {
                        if let Some(resume) = resume {
                            if let Err(e) = save_progress(state, resume.resume).await { return e };
                            if let Some(migrate) = migrate {
                                _ = migrate.send(Some(MigrateChannel { id: state.id, conn: resume.conn }));
                            }
                        } else if let Some(migrate) = migrate {
                            _ = migrate.send(None);
                        }
                        return ChannelEventHandlerState::Exit;
                    },
                    PlayNotification::SaveProgress(resume) => {
                        if save_progress(state, Some(resume)).await.is_err() {
                            error!("{} failed to save progress", state);
                        }
                    }
                },
                Err(e) => {
                    error!("{}: Player exited without notification: {}", state, e);
                    return ChannelEventHandlerState::Unhandled;
                }
            },
            r = state.rx.recv() => match r {
                Some(v) => match v {
                    ChannelEvent::AddSongToQueue { request_id } => if let Err(e) = add_new_request_to_queue(state, request_id).await {
                        return e;
                    },
                    ChannelEvent::RemoveChannel => {
                        _ = state.player.exit();
                        return ChannelEventHandlerState::Exit;
                    }
                },
                None => {
                    // This really should only happen if program is exiting
                    error!("{} event handler channel is closed", state);
                    return ChannelEventHandlerState::Unhandled;
                }
            },
            r = mig.recv() => match r {
                Ok(v) => {
                    migrate = Some(v);
                    _ = state.player.exit();
                },
                Err(_) => unreachable!()
            }
        }
    }
}

async fn add_new_request_to_queue(state: &mut ChannelState, request_id: i64)
    -> std::result::Result<(), ChannelEventHandlerState> {
    if state.last_request_id >= request_id {
        return Ok(());
    }

    let ret = state.serv
        .access_db
        .request_queue(&state.id, state.last_request_id)
        .await;

    let queue = match ret {
        Ok(v) => v,
        Err(e) => {
            error!("{}: Can't fetch request queue: {}", state, e);
            return Err(ChannelEventHandlerState::Unhandled);
        }
    };

    // Updating playlist so client can track songs in queue
    if !queue.is_empty() {
        if let Some(mut playlist) = state.playlist.take() {
            let mut q = Vec::new();
            queue.iter().for_each(|req| {
                q.push(SongInfo {
                    id: req.song.clone(),
                    name: req.title_metadata.clone(),
                    artist: req.artist.clone(),
                    artist_id: req.artist_id.to_string()
                })
            });

            playlist.add_to_queue(q).await;
            state.playlist.replace(playlist);
        }
    }

    for mut request in queue {
        let id       = request.id;
        let mut path = state.serv.media_dir.clone();
        path.push(format!("{}.mp3", request.song));
        // We are sure it's valid
        request.song = path.into_os_string().into_string().unwrap();
        if let Err(e) = state.player.queue_song(request.id, request) {
            error!("{}: Player should not exit before channel event handler: {}", state, e);
            // We should get player notification right after this
            return Ok(());
        };
        state.last_request_id = id;
    }

    Ok(())
}

async fn change_playlist_currenctly_playing(state: &mut ChannelState) {
    if let Some(mut playlist) = state.playlist.take() {
        playlist.song_finished().await;
        state.playlist.replace(playlist);
    }
}

async fn add_automated_request(ch_id: &uuid::Uuid, serv: &ServerState) -> Result<()> {
    // TODO: Add customisation
    let len = serv.access_db
        .request_queue_size(ch_id, -1)
        .await?;

    let add = match len {
        0 => 2,
        1 => 1,
        _ => return Ok(())
    };

    let songs = serv.media_db
        .random_song(add)
        .await?;

    for song in songs {
        add_song_to_queue(serv, ch_id, song, None).await?;
    }

    Ok(())
}

async fn add_song_to_queue(serv: &ServerState, ch_id: &uuid::Uuid,
                           song: SongRequestPicker, user_id: Option<&uuid::Uuid>) -> Result<i64> {
    let ret = serv.access_db
        .channel_add_song_to_queue(
            ch_id,
            &format!("{}/{}", song.album, song.id),
            &song.name,
            &song.artist,
            &song.artist_id,
            user_id
        )
        .await;
    // Caching file in locally so we can serve it right away to user
    if ret.is_ok() {
        let mut path = PathBuf::from(&serv.artwork_dir);
        tokio::spawn(async move {
            path.push(song.album.to_string());
            path.push("artwork0.jpg");
            if let Ok(mut f) = tokio::fs::File::open(path).await {
                _ = f.read_u8().await;
            }
        });
    }
    ret
}

async fn save_progress(state: &mut ChannelState, resume: Option<SongResume>) -> std::result::Result<(), ChannelEventHandlerState> {
    if let Some(resume) = resume {
        if let Err(e) = state.serv.access_db
            .save_playback_progress(resume.request_id, resume.resume_point as i64)
            .await {
            error!("{}: Can't save request progress to db: {}", state, e);
            return Err(ChannelEventHandlerState::Unhandled);
        }
    }
    Ok(())
}

#[derive(Debug)]
pub struct Channel {
    queue: mpsc::UnboundedSender<ChannelEvent>
}

pub async fn create_channel(serv: Arc<ServerState>, user: &UserAuth,
                            add: AddChannel)
    -> Result<AddChannelResp, Response> {
    // Here we only need to verify number of channels for user
    let channel_count = match serv.access_db.user_owned_channels_count(&user.id).await {
        Ok(v) => v,
        Err(_) => return Err(resp::service_unavailable())
    };

    // We only allow creation of channel by users with permission and maximum number
    // of channels lower than MAX value for their permission
    let has_permission = user.permission.can_create_channels() && channel_count >= user.permission.channels_max();
    if !has_permission {
        return Err(resp::forbidden(crate::CHANNEL_CREATION_DENIED));
    }

    // At this point we can create channel
    let ch_id = match serv.access_db.add_channel(
        &wavy_shared::storage::access::AddChannel { name: add.name, description: add.description },
        &user.id,
        &serv.node_id
    ).await {
        Ok(v) => v,
        Err(e) => {
            error!("Failed to create channel for {}: {}", user, e);
            return Err(resp::service_unavailable());
        }
    };

    serv.channels
        .new_channel(ch_id, None, serv.clone())
        .await;

    Ok(AddChannelResp { id: ch_id })
}

pub async fn remove_channel(serv: &ServerState, user: &UserAuth,
                            remove: &RemoveChannel)
    -> Result<RemoveChannelResp, Response> {
    // Only ever permit channel owner to delete it's own channel
    // or this is a manager
    let has_permission = if !user.permission.is_channels_manager() {
        match serv.access_db.user_owns_channel(&user.id, &remove.id).await {
            Ok(is_owner) => is_owner,
            Err(e) => {
                error!("Coudln't check channel owner: {}", e);
                return Err(resp::service_unavailable());
            }
        }
    } else {
        true
    };

    if !has_permission {
        return Err(resp::forbidden(crate::USER_DONT_OWN_CHANNEL));
    }

    if let Err(e) = serv.access_db.remove_channel(&remove.id).await {
        error!("Removing channel failed: {}", e);
        return Err(resp::service_unavailable());
    }

    serv.channels
        .remove_channel(&remove.id, serv)
        .await;

    Ok(RemoveChannelResp {})
}

pub async fn song_request(serv: &ServerState, user: &UserAuth,
                          add: &SongRequest)
    -> Result<SongRequestResp, Response> {
    // Parsing album and song id
    let (album_id, song_id) = parse_song_id(&add.song_id)?;

    // Checking if we didn't surpass limit allowed
    let size = serv.access_db
        .request_queue_size(&add.channel_id, -1)
        .await
        .map_err(|_| resp::failed(crate::INVALID_CHANNEL_ID))?;
    if size as usize >= crate::MAX_CHANNELS_NUMBER {
        return Err(resp::failed(crate::CHANNEL_QUEUE_LIMIT));
    }
    // Fetching song and adding it to channel queue
    // TODO: Prevent duplicates in queue and played in last x time
    let song = serv.media_db
        .request_song(&album_id, song_id)
        .await
        .map_err(|_| resp::service_unavailable())?
        .ok_or(resp::failed(crate::INVALID_SONG_ID))?;

    add_song_to_queue(serv, &add.channel_id, song, Some(&user.id))
        .await
        .map_err(|_| resp::service_unavailable())?;

    Ok(SongRequestResp {})
}

pub fn parse_song_id(song_id: &str) -> Result<(uuid::Uuid, u32), Response> {
    if song_id.len() < 38 {
        return Err(resp::failed(crate::INVALID_SONG_ID));
    }
    let (album_id_str, song_id_str) = song_id.split_at(36);
    let album_id = uuid::Uuid::from_str(album_id_str)
        .map_err(|_| resp::failed(crate::INVALID_SONG_ID))?;
    let song_id = song_id_str[1..].parse::<u32>()
        .map_err(|_| resp::failed(crate::INVALID_SONG_ID))?;

    Ok((album_id, song_id))
}

pub async fn events(serv: &ServerState, req: &EventsRequest)
    -> Result<impl Stream<Item = Result<warp::sse::Event, Infallible>>, Response> {
    // TODO: If we have channel playing in another instance??
    let playlist = serv.playlist
        .get(&req.channel_id)
        .await;
    let playlist = match playlist {
        Some(v) => v,
        None    => return Err(resp::failed(crate::INVALID_CHANNEL_ID))
    };

    let stream = async_stream::stream! {
        let mut reader;
        let lock        = playlist.read().await;
        reader          = lock.update.clone();
        let mut last_id = lock.update_id;
        let ser_qeueue  = lock.queued_ser.clone();
        drop(lock);
        yield Ok(warp::sse::Event::default()
                 .event("init")
                 .data(ser_qeueue.as_ref().to_owned()));
        drop(ser_qeueue);

        loop {
            match reader.recv().await {
                Ok(v) => match v.as_ref() {
                    PlaylistUpdate::SongFinished { update_id } => {
                        // No need to update if we are up to update_id
                        if last_id >= *update_id {
                            continue;
                        }
                        last_id = *update_id;
                        yield Ok(warp::sse::Event::default()
                            .event("pop_queue")
                            .data("."));
                    },
                    PlaylistUpdate::NewInQueue { update_id, ser_songs } => {
                        if last_id >= *update_id {
                            continue;
                        }
                        last_id = *update_id;
                        yield Ok(warp::sse::Event::default()
                            .event("add_queue")
                            .data(ser_songs));
                    }
                },
                Err(qanat::broadcast::RecvError::Lagged) => {
                    // This means that we either just subscribed to broadcast
                    // Or client is reading so slowly that it can't keep up with us
                    let lock = playlist.read().await;
                    if last_id >= lock.update_id {
                        continue;
                    }
                    let ser  = serde_json::to_string(&lock.queued);
                    last_id  = lock.update_id;
                    drop(lock);
                    match ser {
                        Ok(v) => yield Ok(warp::sse::Event::default()
                            .event("init")
                            .data(v)),
                        Err(e) => {
                            error!("Failed to send playlist update to client: {}", e);
                            break;
                        }
                    }
                },
                Err(qanat::broadcast::RecvError::Closed) => break
            }
        };
    };
    Ok(stream)
}
