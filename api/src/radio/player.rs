use std::{collections::VecDeque, fs::File, io::Write, net::TcpStream, sync::Arc, time::Duration};

use anyhow::Result;
use tokio::{io::{AsyncWriteExt, AsyncReadExt, AsyncSeekExt}, sync::{mpsc, oneshot}};
use base64::Engine;
use symphonia::core::{io::MediaSourceStream, meta::MetadataOptions, formats::{FormatOptions, SeekMode, SeekTo}, probe::Hint, units::TimeBase};
use tracing::error;
use wavy_shared::storage::access::ChannelRequest;

use crate::api::ServerState;

enum PlayOrder {
    QueueSong {
        request_id: i64,
        song_path: String,
        resume_point: u64
    }
}

pub enum PlayNotification {
    SongFinished {
        request_id: i64
    },
    SaveProgress(SongResume),
    Stop(Option<MigrateStop>),
    ServerError(Option<SongResume>),
    StorageError {
        resume: Option<SongResume>,
        conn: TcpStream
    }
}

pub struct MigrateStop {
    pub resume: Option<SongResume>,
    pub conn: TcpStream
}

pub struct SongResume {
    pub request_id: i64,
    pub resume_point: u64
}

pub struct Player {
    mount: String,
    tx: mpsc::UnboundedSender<PlayOrder>,
    rx: mpsc::UnboundedReceiver<PlayNotification>,
    stop: Option<oneshot::Sender<()>>,
    metadata: VecDeque<String>
}

type SongInfo = (i64, String, u64);
type SongFhRx = oneshot::Receiver<File>;

enum SongFetch {
    Cached {
        fh: File,
        info: SongInfo,
    },
    Raw {
        info: SongInfo
    }
}

impl Player {
    pub async fn new(serv: Arc<ServerState>, mount: &str) -> Result<Self> {
        // Connect to the radio server
        let conn = Self::connect_radio_server(&serv, mount).await?;

        Ok(Self::from_connection(conn, mount))
    }

    pub fn from_connection(sock: TcpStream, mount: &str) -> Self {
        let (tx, rx)   = mpsc::unbounded_channel();
        let (tx1, rx1) = mpsc::unbounded_channel();
        let (tx2, rx2) = oneshot::channel();
        let rt         = tokio::runtime::Handle::current();
        
        tokio::task::spawn_blocking(move || {
            Self::blocking_media_broadcast(rx, tx1, rx2, sock, rt);
        });

        Self {
            mount: mount.to_string(),
            tx,
            rx: rx1,
            stop: Some(tx2),
            metadata: VecDeque::new()
        }
    }

    pub fn queue_song(&mut self, request_id: i64, request: ChannelRequest) -> Result<()> {
        self.tx.send(PlayOrder::QueueSong {
            request_id,
            song_path: request.song,
            resume_point: request.play_progress as u64
        })?;
        self.metadata.push_back(format!("{} - {}", request.artist, request.title_metadata));
        Ok(())
    }

    pub async fn playback_notification(&mut self) -> Result<PlayNotification> {
        match self.rx.recv().await {
            Some(v) => Ok(v),
            None => Err(anyhow::Error::msg("Playback worker exited"))
        }
    }

    pub fn exit(&mut self) -> Result<()> {
        if let Some(stop) = self.stop.take() {
            stop.send(())
                .map_err(|_| anyhow::Error::msg("Playback worker exited"))?;
        }

        Ok(())
    }

    pub async fn update_stream_metadata(&mut self, serv: &ServerState) -> Result<()> {
        let title = match self.metadata.pop_front() {
            Some(v) => v,
            None => return Err(anyhow::Error::msg("No new metadata left to transmit"))
        };
        loop {
            if let Err(e) = self.update_stream_metadata_inner(serv, &title).await {
                error!("Failed to reach radio server: {}", e);
                tokio::time::sleep(Duration::from_millis(crate::CHANNEL_ERROR_TIMEOUT)).await;
                continue;
            }
            break;
        }

        Ok(())
    }

    async fn update_stream_metadata_inner(&mut self, serv: &ServerState, title: &str) -> Result<()> {
        let mut sock = tokio::net::TcpStream::connect(&serv.radio_server)
            .await?;

        let bs64 = base64::engine::general_purpose::STANDARD;

        sock.write_all(format!("\
GET /admin/metadata?mode=updinfo&mount={}&url=&song={} HTTP/1.1\r\n\
Host: {}\r\n\
Authorization: Basic {}\r\n\
Connection: close\r\n\r\n",
            self.mount,
            urlencoding::encode(title),
            serv.radio_server,
            bs64.encode(&serv.radio_source_auth)
        ).as_bytes())
            .await?;

        // We don't care about response

        Ok(())
    }

    async fn connect_radio_server(serv: &ServerState, mount: &str) -> Result<TcpStream> {
        let mut sock = tokio::net::TcpStream::connect(&serv.radio_server)
            .await?;

        let bs64 = base64::engine::general_purpose::STANDARD;

        sock.write_all(format!("\
SOURCE {} HTTP/1.1\r\n\
Host: {}\r\n\
Content-Type: audio/mpeg\r\n\
Authorization: Basic {}\r\n\
Connection: close\r\n\
Icy-Metadata: 1\r\n\
Ice-Name: {}\r\n\
Ice-Genre: {}\r\n\
Ice-Bitrate: 320\r\n\
Ice-Description: {}\r\n\
Ice-Public: 1\r\n\r\n",
            mount,
            serv.radio_server,
            bs64.encode(&serv.radio_source_auth),
            serv.server_properties.name,
            serv.server_properties.genre,
            serv.server_properties.desc
        ).as_bytes())
            .await?;

        let headers = Self::read_headers(&mut sock).await?;

        if !headers.starts_with(b"HTTP/1.1 200 OK") {
            return Err(anyhow::Error::msg("Can't authenticate to radio server"));
        }

        Ok(sock.into_std()?)
    }

    async fn read_headers(sock: &mut tokio::net::TcpStream) -> Result<Vec<u8>> {
        let mut buf     = [0u8; 1];
        let mut headers = Vec::new();
        loop {
            sock.read_exact(&mut buf)
                .await?;
            headers.push(buf[0]);

            if headers.windows(4).last() == Some(b"\r\n\r\n") {
                break;
            }
        };

        Ok(headers)
    }

    /// Method used to cache audio file in local machine before playing it
    /// Most remote filesystems will usually cache a file if it's requested to be read
    async fn cache_song_file(file: String, tx: oneshot::Sender<std::fs::File>) {
        // Settings a timeout
        // We don't want to do this forever
        _ = tokio::time::timeout(Duration::from_secs(50), async move {
            let mut buf = [0u8; 8192];
            loop {
                if let Ok(mut f) = tokio::fs::File::open(&file).await {
                    // Reading a chunk from file
                    // This should be enough to cache it?
                    if f.read_exact(&mut buf).await.is_ok() && f.seek(std::io::SeekFrom::Start(0)).await.is_ok() {
                        _ = tx.send(f.into_std().await);
                        break;
                    }
                }
            };
        }).await;
    }

    fn get_next_queued(rx: &mut mpsc::UnboundedReceiver<PlayOrder>,
                       next: &mut Option<(SongInfo, SongFhRx)>,
                       rt: &mut tokio::runtime::Handle)
        -> Option<SongFetch> {
        let ret = match rx.blocking_recv() {
            Some(PlayOrder::QueueSong { request_id, song_path, resume_point }) => (request_id, song_path, resume_point),
            // Should never happen
            // We will always get notification if channel is closed
            None => return None
        };
        
        // If this is first song to be queued we directly return it
        // otherwise we cache next song and return one we need to play
        let (tx, fs_rx) = oneshot::channel();
        match next.take() {
            Some((n, mut nfs_rx)) => {
                rt.spawn(Self::cache_song_file(ret.1.clone(), tx));
                next.replace((ret, fs_rx));
                Some(match nfs_rx.try_recv() {
                    Ok(fh) => SongFetch::Cached { fh, info: n },
                    Err(_) => SongFetch::Raw { info: n }
                })
            },
            None => {
                let n = match rx.blocking_recv() {
                    Some(PlayOrder::QueueSong { request_id, song_path, resume_point }) => (request_id, song_path, resume_point),
                    None => return None
                };
                rt.spawn(Self::cache_song_file(n.1.clone(), tx));
                next.replace((n, fs_rx));
                Some(SongFetch::Raw { info: ret })
            }
        }
    }

    fn blocking_media_broadcast(mut rx: mpsc::UnboundedReceiver<PlayOrder>,
                                tx: mpsc::UnboundedSender<PlayNotification>,
                                mut stop: oneshot::Receiver<()>,
                                sock: TcpStream, mut rt: tokio::runtime::Handle) {
        // We might need it since socket was async before
        if let Err(e) = sock.set_nonblocking(false) {
            error!("Failed to set radio server socket to blocking: {}", e);
            _ = tx.send(PlayNotification::ServerError(None));
            return;
        }

        let mut prog;
        let mut sock = SendTimeSync::new(sock);
        let mut next = None;
        loop {
            match stop.try_recv() {
                Ok(_) => {
                    _ = tx.send(PlayNotification::Stop(None));
                    return;
                },
                Err(oneshot::error::TryRecvError::Empty) => (),
                // Should never happen
                Err(oneshot::error::TryRecvError::Closed) => return
            }

            let (request_id, song_path, resume_point, file) = match Self::get_next_queued(&mut rx, &mut next, &mut rt) {
                Some(v) => match v {
                    SongFetch::Raw { info } => (info.0, info.1, info.2, None),
                    SongFetch::Cached { fh, info } => (info.0, info.1, info.2, Some(fh))
                },
                None => return
            };

            let file = match file {
                Some(v) => v,
                None    => match std::fs::File::open(&song_path) {
                    Ok(v) => v,
                    Err(e) => {
                        error!("Failed reading song: {}", e);
                        _ = tx.send(PlayNotification::StorageError { resume: None, conn: sock.sock });
                        return;
                    }
                }
            };
            let mss  = MediaSourceStream::new(Box::new(file), Default::default());
            let hint = Hint::new();

            // Use the default options for metadata and format readers.
            let meta_opts: MetadataOptions  = Default::default();
            let fmt_opts: FormatOptions     = FormatOptions::default();
            

            let probed = match symphonia::default::get_probe()
                .format(&hint, mss, &fmt_opts, &meta_opts) {
                Ok(v) => v,
                Err(e) => match e {
                    e @ (symphonia::core::errors::Error::Unsupported(_) | symphonia::core::errors::Error::DecodeError(_)) => {
                        error!("Skipping song {song_path} due to: {e}");
                        // Notifying that we finished reading song to move to next one
                        if tx.send(PlayNotification::SongFinished { request_id }).is_err() {
                            return;
                        }
                        continue;
                    },
                    e => {
                        error!("Failed probing song {song_path}: {}", e);
                        _ = tx.send(PlayNotification::StorageError { resume: None, conn: sock.sock });
                        return;
                    }
                }
            };

            let mut format = probed.format;
            if resume_point > 0 {
                if let Err(e) = format.seek(SeekMode::Accurate, SeekTo::TimeStamp { ts: resume_point, track_id: 0 }) {
                    error!("Failed seeking song {song_path}: {}", e);
                    _ = tx.send(PlayNotification::StorageError { resume: None, conn: sock.sock });
                    return;
                }
                // We actually save last written packet to server so we need to read it here
                _ = format.next_packet();
            }

            // Fetching timebase to calculate durations
            if let Some(track) = format.default_track() {
                if let Some(timebase) = track.codec_params.time_base {
                    sock.timebase = timebase
                }
            }

            prog       = 0;
            let mut ts = None;
            loop {
                match format.next_packet() {
                    Ok(packet) => {
                        if sock.send(packet.buf(), packet.dur).is_err() {
                            _ = tx.send(PlayNotification::ServerError(ts.map(|ts| SongResume { request_id, resume_point: ts })));
                            return;
                        }
                        ts = Some(packet.ts());
                        if sock.timebase.calc_time(packet.ts()).seconds > prog {
                            prog += crate::CHANNEL_UPDATE_PROGRESS_INT;
                            _ = tx.send(PlayNotification::SaveProgress(SongResume { request_id, resume_point: packet.ts() }));
                        }
                        sock.sync();
                    },
                    Err(e) => match e {
                        symphonia::core::errors::Error::IoError(err) if err.kind() == std::io::ErrorKind::UnexpectedEof
                            && err.to_string() == "end of stream" => {
                            // This is actually not an error but we finished reading song
                            if tx.send(PlayNotification::SongFinished { request_id }).is_err() {
                                // If we can't reach main channel task we better stop here
                                return;
                            }
                            // Reading next song
                            break;
                        },
                        e => {
                            error!("Player failed reading song {song_path}: {e}");
                            // Here we just skip song, retrying to play it may end us in a loop
                            if tx.send(PlayNotification::SongFinished { request_id }).is_err() {
                                return;
                            }
                            break;
                        }
                    }
                }

                // We also check if we need to stop here
                match stop.try_recv() {
                    Ok(_) => {
                        _ = tx.send(PlayNotification::Stop(Some(MigrateStop { resume: ts.map(|ts| SongResume { request_id, resume_point: ts }), conn: sock.sock })));
                        return;
                    },
                    Err(oneshot::error::TryRecvError::Empty) => (),
                    // Should never happen
                    Err(oneshot::error::TryRecvError::Closed) => return
                }
            };
        };
    }
}

struct SendTimeSync {
    send_time: f64,
    start_time: i64,
    sock: TcpStream,
    timebase: TimeBase
}

impl SendTimeSync {
    /*
    Courtesy of the libshout project
    https://gitlab.xiph.org/xiph/icecast-libshout/-/blob/master/src/shout.c#L263
    */
    fn new(sock: TcpStream) -> Self {
        Self {
            timebase: TimeBase { numer: 1, denom: 44100 },
            send_time: 0.0,
            start_time: chrono::offset::Utc::now().timestamp_micros(),
            sock
        }
    }

    fn send(&mut self, buf: &[u8], dur: u64) -> std::io::Result<()> {
        self.send_time += self.timebase.calc_time(dur).frac;
        self.sock.write_all(buf)
    }

    fn sync(&mut self) {
        let sleep = (self.send_time * 1000000f64) as i64 - (chrono::offset::Utc::now().timestamp_micros() - self.start_time);
        if sleep > 0 {
            std::thread::sleep(Duration::from_micros(sleep as u64));
        }
    }
}
