use std::{num::NonZeroUsize, sync::Arc, collections::VecDeque};
use hashbrown::HashMap;

use qanat::broadcast;
use serde::Serialize;
use tokio::sync::RwLock;
use uuid::Uuid;
use tracing::error;


pub struct ChannelsPlaylist {
    map: RwLock<HashMap<Uuid, Arc<RwLock<ChannelPlaylist>>>>
}

impl Default for ChannelsPlaylist {
    fn default() -> Self {
        Self {
            map: RwLock::new(HashMap::new())
        }
    }
}

impl ChannelsPlaylist {
    // TODO: We are missing how to handle reading playlist if channel is not in this instance
    pub async fn add(&self, ch_id: uuid::Uuid) -> Option<ChannelPlaylistWriter> {
        let (tx, rx) = broadcast::channel(NonZeroUsize::try_from(100).expect("Should be bigger than 0"));
        let playlist = Arc::new(RwLock::new(ChannelPlaylist {
            queued: VecDeque::new(),
            queued_ser: Arc::new("[]".to_string()),
            update_id: 0,
            update: rx
        }));
        let ret      = self.map.write()
            .await
            .try_insert(ch_id, playlist.clone())
            .is_ok();
        match ret {
            true  => Some(ChannelPlaylistWriter { data: playlist, tx }),
            false => None
        }
    }

    pub async fn remove(&self, ch_id: &uuid::Uuid) {
        self.map.write()
            .await
            .remove(ch_id);
    }

    pub async fn get(&self, ch_id: &uuid::Uuid) -> Option<Arc<RwLock<ChannelPlaylist>>> {
        self.map
            .read()
            .await
            .get(ch_id)
            .cloned()
    }
}

pub struct ChannelPlaylistWriter {
    pub data: Arc<RwLock<ChannelPlaylist>>,
    pub tx: broadcast::Sender<Arc<PlaylistUpdate>>
}

impl ChannelPlaylistWriter {
    pub async fn song_finished(&mut self) {
        let update_id;
        {
            let mut lock = self.data
                .write()
                .await;
            if lock.queued.is_empty() {
                return;
            }
            lock.queued
                .pop_front();
            match serde_json::to_string(&lock.queued) {
                Ok(v) => lock.queued_ser = Arc::new(v),
                Err(e) => error!("Failed to serialize channel playlist: {}", e)
            }
            lock.update_id += 1;
            update_id       = lock.update_id;
        }
        self.tx
            .send(Arc::new(PlaylistUpdate::SongFinished { update_id }));
    }

    pub async fn add_to_queue(&mut self, songs: Vec<SongInfo>) {
        let update_id;
        {
            let mut lock = self.data
                .write()
                .await;
            songs.iter()
                .for_each(|x| lock.queued.push_back(x.clone()));
            match serde_json::to_string(&lock.queued) {
                Ok(v) => lock.queued_ser = Arc::new(v),
                Err(e) => error!("Failed to serialize channel playlist: {}", e)
            }
            lock.update_id += 1;
            update_id       = lock.update_id;
        }
        match serde_json::to_string(&songs) {
            Ok(ser) => self.tx
                .send(Arc::new(PlaylistUpdate::NewInQueue { update_id, ser_songs: ser })),
            Err(e) => error!("Failed to send new channel playlist update: {}", e)
        }
    }
}

pub struct ChannelPlaylist {
    pub queued: VecDeque<SongInfo>,
    pub queued_ser: Arc<String>,
    pub update_id: usize,
    pub update: broadcast::Receiver<Arc<PlaylistUpdate>>
}

#[derive(Debug, Clone, Serialize)]
pub struct SongInfo {
    pub id: String,
    pub name: String,
    pub artist: String,
    pub artist_id: String
}

#[repr(u8)]
#[derive(Clone)]
pub enum PlaylistUpdate {
    SongFinished { update_id: usize },
    NewInQueue { update_id: usize, ser_songs: String }
}
