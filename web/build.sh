#!/bin/bash

trunk build --release

SOURCE_DIR="assets"
DESTINATION_DIR="dist"

if [ ! -d "$SOURCE_DIR" ]; then
    echo "Dist directory does not exist."
    exit 1
fi

for FILE in "$SOURCE_DIR"/*; do
    if [ -f "$FILE" ]; then
        BASENAME=$(basename "$FILE")
        if [[ "$FILE" == *.png || "$FILE" == *.css || "$FILE" == *.svg || "$FILE" == *.mp3 ]]; then
            HASH=$(sha256sum "$FILE" | awk '{ print substr($1, 1, 16) }')
            NEW_FILENAME="${BASENAME%.*}-$HASH.${BASENAME##*.}"
        else
            NEW_FILENAME="${BASENAME%.*}.${BASENAME##*.}"
        fi
        cp "$FILE" "$DESTINATION_DIR/$NEW_FILENAME"
    fi
done
