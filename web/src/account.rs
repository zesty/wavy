use anyhow::{anyhow, Result};
use gloo_net::http;
use dioxus::prelude::*;
use serde::Serialize;
use web_sys::RequestCredentials;
use crate::utils::ErrorMsg;

use crate::ui::{toast::ToastAction, asset::*};

#[derive(Debug, Serialize)]
pub struct LoginCredentials {
    pub user: String,
    pub pass: String
}

pub async fn login(creds: LoginCredentials, toast: Coroutine<ToastAction>) -> Result<()> {
    let req = serde_urlencoded::to_string(&creds)?;

    let resp = http::Request::post(&format!("{}api/v1/login", crate::SERVER_DEFAULT_VALS.path))
        .credentials(RequestCredentials::Include)
        .header("Content-Type", "application/x-www-form-urlencoded")
        .body(req)
        .unwrap()
        .send()
        .await;
    
    match resp {
        Ok(v) => if v.status() == 200 {
            toast.send(ToastAction { message: "Successfully logged in!".to_string() });
            Ok(())
        } else {
            match v.json::<ErrorMsg>().await {
                Ok(v) => {
                    toast.send(ToastAction { message: v.error });
                    Err(anyhow!("Login failed with wrong response from server"))
                },
                Err(e) => {
                    toast.send(ToastAction { message: "Server sent wrong response".to_string() });
                    Err(e.into())
                }
            }
        },
        Err(e) => {
            toast.send(ToastAction { message: "Can't contact server, rerty later.".to_string() });
            Err(e.into())
        }
    }
}

pub fn logout(toast: Coroutine<ToastAction>, mut login_panel: Signal<&'static str>) {
    spawn(async move {
        let resp = http::Request::get(&format!("{}api/v1/logout", crate::SERVER_DEFAULT_VALS.path))
            .credentials(RequestCredentials::Include)
            .send()
            .await;
        match resp {
            Ok(v) => match v.status() {
                200 => {
                    toast.send(ToastAction { message: "Successfully logged out. See you soon!".to_string() });
                    login_panel.set(LOGIN_ICON);
                },
                401 => {
                    toast.send(ToastAction { message: "You are not logged in, perhaps session expired?".to_string() });
                    login_panel.set(LOGIN_ICON);
                },
                _   => {
                    toast.send(ToastAction { message: "Logout failed? Server is acting funny...".to_string() });
                }
            },
            Err(_) => {
                toast.send(ToastAction { message: "Can't contact server to logout.".to_string() });
            }
        }
    });
}

pub fn check_status(mut login_panel: Signal<&'static str>) {
    spawn(async move {
        let resp = http::Request::get(&format!("{}api/v1/status", crate::SERVER_DEFAULT_VALS.path))
            .credentials(RequestCredentials::Include)
            .send()
            .await;
        
        let doc = web_sys::window()
            .unwrap()
            .document()
            .unwrap();

        if let Ok(v) = resp {
            if v.status() == 200 {
                login_panel.set(LOGOUT_ICON);
            }
        }

        doc.get_element_by_id("top-panel")
            .unwrap()
            .set_class_name("top-panel");
    });
}
