use gloo_net::http;
use dioxus::prelude::*;
use serde::{Deserialize, Serialize};
use web_sys::RequestCredentials;

use crate::ui::{artist::ArtistProps, toast::ToastAction};

#[derive(Debug, Serialize, Clone)]
pub struct Search {
    pub query: String,
    #[serde(rename = "type")]
    pub stype: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub page: Option<String>,
    pub page_size: u32,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub rev: Option<bool>
}

#[derive(Debug, Deserialize)]
struct SearchResult {
    result: Vec<SearchEntry>
}

#[derive(Debug, Deserialize, PartialEq, Clone)]
pub struct SearchEntry {
    pub artist_name: String,
    pub artist_id: String,
    pub album_name: Option<String>,
    pub album_id: Option<String>,
    pub song_name: Option<String>,
    pub song_id: Option<u32>
}

pub enum SearchStatus {
    Good,
    NoMatch,
    Error
}

#[derive(Debug, Serialize, Clone)]
pub struct ArtistPageReq {
    pub artist_id: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub page: Option<String>,
    pub page_size: u32,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub rev: Option<bool>
}

#[derive(Debug, Deserialize)]
struct ArtistPageResp {
    pub name: String,
    pub albums: Vec<AlbumDisplay>,
    pub total_pages: u32
}

#[derive(Debug, Deserialize, Clone, PartialEq)]
pub struct AlbumDisplay {
    pub name: String,
    pub id: String,
    pub songs: Vec<SongDisplay>
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct SongDisplay {
    pub id: u32,
    pub name: String,
    pub duration: i64
}

pub async fn find_match(search: Search, mut search_result: Signal<Vec<SearchEntry>>, toast: Coroutine<ToastAction>) -> SearchStatus {
    let ser = serde_json::to_string(&search).unwrap();

    let resp = http::Request::post(&format!("{}api/v1/search", crate::SERVER_DEFAULT_VALS.path))
        .credentials(RequestCredentials::Include)
        .header("Content-Type", "application/json")
        .body(&ser)
        .unwrap()
        .send()
        .await;
    match resp {
        Ok(v) => match v.text().await.map(|r| serde_json::from_str::<SearchResult>(&r)) {
            Ok(Ok(mut v)) => {
                if v.result.is_empty() {
                    toast.send(ToastAction { message: "No result matches your query".to_owned() });
                    return SearchStatus::NoMatch
                }
                if search.rev.eq(&Some(true)) {
                    v.result.reverse();
                }
                let mut w = search_result.write();
                w.clear();
                w.append(&mut v.result);
                drop(w);
                return SearchStatus::Good
            },
            _ => {
                toast.send(ToastAction { message: "Server returned wrong response for search query".to_owned() });
            }
        },
        Err(_) => {
            toast.send(ToastAction { message: "Can't send search request to server.".to_string() });
        }
    }

    SearchStatus::Error
}

pub async fn get_artist_page(search: ArtistPageReq, result: &mut ArtistProps, toast: Coroutine<ToastAction>) -> Option<u32> {
    let ser = serde_json::to_string(&search).unwrap();

    let resp = http::Request::post(&format!("{}api/v1/artist", crate::SERVER_DEFAULT_VALS.path))
        .credentials(RequestCredentials::Include)
        .header("Content-Type", "application/json")
        .body(&ser)
        .unwrap()
        .send()
        .await;
    match resp {
        Ok(v) => match v.text().await.map(|r| serde_json::from_str::<ArtistPageResp>(&r)) {
            Ok(Ok(mut v)) => {
                if v.albums.is_empty() {
                    return None;
                }
                if search.rev.eq(&Some(true)) {
                    v.albums.reverse();
                }
                result.name.set(v.name);
                let mut w = result.result.write();
                w.clear();
                w.append(&mut v.albums);
                drop(w);
                return Some(v.total_pages)
            },
            _ => {
                toast.send(ToastAction { message: "Can't find artist with given id!?".to_owned() });
            }
        },
        Err(_) => {
            toast.send(ToastAction { message: "Can't send search request to server.".to_string() });
        }
    }

    None
}
