#![allow(non_snake_case)]
mod channel;
mod utils;
mod ui;
mod account;
mod search;

use dioxus::prelude::*;
use serde::Deserialize;
#[cfg(debug_assertions)]
use tracing::Level;
use ui::home::Home;
use once_cell::sync::Lazy;

#[derive(Deserialize)]
pub struct ServerValues {
    pub path: String,
    pub default_channel: String
}

pub static SERVER_DEFAULT_VALS: Lazy<ServerValues> = Lazy::new(||
    serde_json::from_str(include_str!("../web.server")).unwrap()
);

#[derive(Clone, Routable, Debug, PartialEq)]
enum Route {
    #[route("/")]
    Home {},
}

fn main() {
    // Init logger
    #[cfg(debug_assertions)]
    dioxus_logger::init(Level::INFO).expect("failed to init logger");
    launch(App);
}

fn App() -> Element {
    rsx! {
        Router::<Route> {}
    }
}


