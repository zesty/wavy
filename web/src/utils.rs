use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct ErrorMsg {
    pub error: String
}
