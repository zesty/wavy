use std::collections::VecDeque;

use anyhow::{anyhow, Result};
use dioxus::prelude::*;
use futures::StreamExt;
use gloo_net::{eventsource::futures::EventSource, http};
use gloo_timers::future::TimeoutFuture;
use serde::{Deserialize, Serialize};
use web_sys::{js_sys::Array, MediaImage, MediaMetadata, RequestCredentials, MediaSession};

use crate::{ui::toast::ToastAction, utils::ErrorMsg};

pub struct ChannelStateReader {
    tx: Coroutine<()>,
}

#[derive(Debug, Deserialize)]
pub struct SongInfo {
    pub id: String,
    pub name: String,
    pub artist: String,
    pub artist_id: String
}
 
#[derive(PartialEq, Props, Clone)]
pub struct Song {
    pub name: String,
    pub artist: String,
    pub artist_id: String,
    pub artwork: String
}

impl Song {
    pub fn new() -> Self {
        Self {
            name: "".to_string(),
            artist: "".to_string(),
            artist_id: "".to_string(),
            artwork: "".to_string()
        }
    }
}

impl ChannelStateReader {
    pub fn new(mut current: Signal<Song>, mut next: Signal<VecDeque<Song>>, ch_id: String) -> Self {
        let tx = use_coroutine(move |mut rx| async move {
            // Loop and wait for the next message
            if let Some(_request) = rx.next().await {
                loop {
                    _ = Self::reader(&mut current, &mut next, &ch_id).await;
                    TimeoutFuture::new(5000).await;
                }
            }
        });
        tx.send(());

        Self { tx }
    }

    async fn reader(
        current: &mut Signal<Song>,
        next: &mut Signal<VecDeque<Song>>,
        ch_id: &str,
    ) -> Result<()> {
        let es = web_sys::EventSource::new_with_event_source_init_dict(
            &format!("{}api/v1/channelevents/{ch_id}", crate::SERVER_DEFAULT_VALS.path),
            web_sys::EventSourceInit::new().with_credentials(true)
        ).map_err(|_| anyhow::Error::msg("Failed to create event source object"))?;

        let mediasession = web_sys::window().unwrap().navigator().media_session();

        let mut es = EventSource { es };
        let init = es.subscribe("init")?;
        let pop_queue = es.subscribe("pop_queue")?;
        let add_queue = es.subscribe("add_queue")?;
        let mut stream = futures::stream_select!(init, pop_queue, add_queue);
        while let Some(Ok((event, msg))) = stream.next().await {
            match event.as_str() {
                "init" => {
                    if let Ok(mut playlist) =
                        serde_json::from_str::<Vec<SongInfo>>(&msg.data().as_string().unwrap())
                    {
                        let playing   = playlist.remove(0);
                        let mut cur   = current.write();
                        cur.artist_id = playing.artist_id;
                        cur.name      = playing.name;
                        cur.artist    = playing.artist;
                        cur.artwork   = format!(
                            "{}api/v1/songartwork/{}",
                            crate::SERVER_DEFAULT_VALS.path,
                            match playing.id.split('/').next() {
                                Some(v) => v,
                                None => &playing.id
                            }
                        );
                        Self::update_media_session_metadata(&mediasession, &cur)?;
                        drop(cur);
                        next.write().clear();
                        Self::add_to_next(next, playlist)
                    }
                }
                "pop_queue" => {
                    let playing   = next.write().remove(0).unwrap();
                    let mut cur   = current.write();
                    cur.artist_id = playing.artist_id;
                    cur.name      = playing.name;
                    cur.artist    = playing.artist;
                    cur.artwork   = playing.artwork;
                    Self::update_media_session_metadata(&mediasession, &cur)?;
                }
                "add_queue" => {
                    if let Ok(v) =
                        serde_json::from_str::<Vec<SongInfo>>(&msg.data().as_string().unwrap())
                    {
                        Self::add_to_next(next, v)
                    }
                }
                _ => (),
            }
        }

        Ok(())
    }

    fn update_media_session_metadata(mediasession: &MediaSession, cur: &Song) -> Result<()> {
        let metadata = MediaMetadata::new().map_err(|_| anyhow!("Error creating MediaMetadata"))?;
        metadata.set_title(&cur.name);
        metadata.set_artist(&cur.artist);
        let artwork = Array::new_with_length(1);
        artwork.set(0, MediaImage::new(&cur.artwork).into());
        metadata.set_artwork(&artwork);
        mediasession.set_metadata(Some(&metadata));

        Ok(())
    }

    fn add_to_next(next: &mut Signal<VecDeque<Song>>, mut playlist: Vec<SongInfo>) {
        let mut next = next.write();
        playlist.drain(0..)
            .for_each(|s| next.push_back(Song {
                name: s.name,
                artist: s.artist,
                artist_id: s.artist_id,
                artwork: format!(
                    "{}api/v1/songartwork/{}",
                    crate::SERVER_DEFAULT_VALS.path,
                    match s.id.split('/').next() {
                        Some(v) => v,
                        None => &s.id
                    }
                )
            }));
        drop(next);
    }
}

#[derive(Debug, Serialize)]
pub struct AddToQueue {
    pub channel_id: String,
    pub song_id: String
}

pub fn queue_song(add: AddToQueue, toast: Coroutine<ToastAction>) {
    spawn(async move {
        let ser = serde_json::to_string(&add).unwrap();

        let resp = http::Request::post(&format!("{}api/v1/songrequest", crate::SERVER_DEFAULT_VALS.path))
            .credentials(RequestCredentials::Include)
            .header("Content-Type", "application/json")
            .body(&ser)
            .unwrap()
            .send()
            .await;
        match resp {
            Ok(v) => match v.text().await.map(|r| serde_json::from_str::<ErrorMsg>(&r)) {
                Ok(Err(_)) => {
                    toast.send(ToastAction { message: "Song added to queue!".to_owned() });
                }
                Ok(Ok(e)) => {
                    toast.send(ToastAction { message: e.error });
                },
                _ => {
                    toast.send(ToastAction { message: "Server returned wrong response for search query".to_owned() });
                }
            },
            Err(_) => {
                toast.send(ToastAction { message: "Can't send add to playlist request to server.".to_string() });
            }
        }
    });
}
