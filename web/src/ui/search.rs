use dioxus::prelude::*;

use crate::{
    channel::{queue_song, AddToQueue},
    search::{find_match, Search, SearchEntry, SearchStatus},
    SERVER_DEFAULT_VALS,
    ui::asset::*
};

use super::{artist::{load_artist_page_init, ArtistProps}, home::AsideProps, toast::ToastAction, load::{LoadWheel, LazyLoadImage}};

#[component]
pub fn SearchPanel(search_hide: Signal<&'static str>, toast: Coroutine<ToastAction>,
                   aside_props: AsideProps, artist_props: ArtistProps) -> Element {
    let mut search_type     = use_signal(|| "song".to_owned());
    let mut search_query    = use_signal(|| "".to_owned());
    let mut last_search     = use_signal(|| None as Option<Search>);
    let search_result       = use_signal(Vec::<SearchEntry>::new);
    let buttons_visible     = use_signal(|| "hide-item-n");
    let prev_visible        = use_signal(|| "");
    let next_visible        = use_signal(|| "");
    let loading             = use_signal(|| false);

    rsx!(
        div {
            class: "aside-page {search_hide}",
            div {
                class: "in-search",
                input {
                    name: "pass",
                    placeholder: "Type a search query...",
                    "type": "input",
                    required: "true",
                    enterkeyhint: "done",
                    oninput: move |event| search_query.set(event.value()),
                    onkeydown: move |event| if event.key().eq(&Key::Enter) {
                        if !artist_props.login_panel.read().eq(LOGOUT_ICON) {
                            toast.send(ToastAction { message: "You need to be logged in to perform a search query".to_owned() });
                            return;
                        }

                        let search = Search {
                            query: search_query.read().to_string(),
                            stype: search_type.read().to_string(),
                            page: None,
                            page_size: 50,
                            rev: None
                        };
                        last_search.set(Some(search.clone()));

                        load_next_page(
                            search,
                            search_result,
                            toast,
                            buttons_visible,
                            prev_visible,
                            next_visible,
                            loading
                        );
                    }
                }
                select {
                    style: "background-image: url({DOWN_ICON});",
                    onchange: move |event| search_type.set(event.data.value()),
                    option { "song" }
                    option { "album" }
                    option { "artist" }
                }
            }
            if loading.read().eq(&true) {
                LoadWheel {}
            } else {
                ul {
                    id: "search-results",
                    class: "listing-box",
                    for n in search_result() {
                        SearchResultView { r: n, a: aside_props, artist_props: artist_props, toast: toast }
                    }
                }
                div {
                    class: "page-buttons {buttons_visible}",
                    img {
                        class: "nav-button {prev_visible}",
                        src: PREV_ICON,
                        onclick: move |_| {
                            if let Some(mut search) = last_search.read().clone() {
                                if let Some(first) = search_result.read().first() {
                                    search.rev  = Some(true);
                                    search.page = match search.stype.as_str() {
                                        "album" => first.album_id.clone(),
                                        "song" => match (first.album_id.as_ref(), first.song_id.as_ref()) {
                                            (Some(a), Some(s)) => Some(format!("{}/{}", a, s)),
                                            _ => None
                                        },
                                        _ => Some(first.artist_id.clone())
                                    };
                                    load_next_page(
                                        search,
                                        search_result,
                                        toast,
                                        buttons_visible,
                                        prev_visible,
                                        next_visible,
                                        loading
                                    );
                                }
                            }
                        }
                    }
                    img {
                        class: "nav-button {next_visible}",
                        src: NEXT_ICON,
                        onclick: move |_| {
                            if let Some(mut search) = last_search.read().clone() {
                                if let Some(last) = search_result.read().last() {
                                    search.rev = None;
                                    search.page = match search.stype.as_str() {
                                        "album" => last.album_id.clone(),
                                        "song" => match (last.album_id.as_ref(), last.song_id.as_ref()) {
                                            (Some(a), Some(s)) => Some(format!("{}/{}", a, s)),
                                            _ => None
                                        },
                                        _ => Some(last.artist_id.clone())
                                    };
                                    load_next_page(
                                        search,
                                        search_result,
                                        toast,
                                        buttons_visible,
                                        prev_visible,
                                        next_visible,
                                        loading
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }
    )
}

fn load_next_page(search: Search, search_result: Signal<Vec<SearchEntry>>,
                  toast: Coroutine<ToastAction>, mut buttons_visible: Signal<&'static str>,
                  mut prev_visible: Signal<&'static str>, mut next_visible: Signal<&'static str>,
                  mut loading: Signal<bool>) {

    loading.set(true);
    spawn(async move {
        let first = search.page.is_none();
        let back  = search.rev.is_some_and(|r| r);
        let ret   = find_match(
            search,
            search_result,
            toast
        ).await;

        loading.set(false);
        match ret {
            SearchStatus::NoMatch => {
                if first {
                    buttons_visible.set("hide-item-n");
                } else {
                    toast.send(ToastAction {
                        message: if back {
                            "You are at the front page already"
                        } else {
                            "Last page already reached"
                        }.to_owned()
                    });
                    prev_visible.set("hide-item-n");
                }
            },
            SearchStatus::Good => {
                buttons_visible.set("");
                if first {
                    prev_visible.set("hide-item-n");
                } else {
                    prev_visible.set("");
                }
                if search_result.read().len() == 50 {
                    next_visible.set("");
                } else {
                    next_visible.set("hide-item-n");
                }
            },
            SearchStatus::Error => ()
        }
    });
}

#[component]
fn SearchResultView(r: SearchEntry, a: AsideProps,
                    artist_props: ArtistProps, toast: Coroutine<ToastAction>) -> Element {
    match (r.artist_id, r.album_id, r.album_name, r.song_id, r.song_name) {
        (id, None, None, None, None) => rsx!(
            li {
                class: "artist-only",
                p {
                    class: "aside-song-title clickable",
                    onclick: move |_| {
                        load_artist_page_init(id.clone(), a, artist_props, toast);
                    },
                    "{r.artist_name}"
                }
            }
        ),
        (id, Some(aid), Some(aname), None, None) => rsx!(
            li {
                class: "playlist-element",
                LazyLoadImage { class: "aside-song-art", src: "/api/v1/songartwork/{aid}" }
                div {
                    p {
                        class: "aside-song-title clickable",
                        onclick: move |_| {
                            load_artist_page_init(id.clone(), a, artist_props, toast);
                        },
                        "{r.artist_name}"
                    }
                    p {
                        class: "aside-song-title",
                        "{aname}"
                    }
                }
            }
        ),
        (id, Some(aid), _, Some(sid), Some(sname)) => rsx!(
            li {
                class: "playable-element",
                LazyLoadImage { class: "aside-song-art", src: "/api/v1/songartwork/{aid}" }
                div {
                    p {
                        class: "aside-song-title clickable",
                        onclick: move |_| {
                            load_artist_page_init(id.clone(), a, artist_props, toast);
                        },
                        "{r.artist_name}"
                    }
                    p {
                        class: "aside-song-title",
                        "{sname}"
                    }
                }
                img {
                    src: ADD_TO_PL_ICON,
                    onclick: move |_| {
                        queue_song(
                            AddToQueue {
                                channel_id: SERVER_DEFAULT_VALS.default_channel.clone(),
                                song_id: format!("{aid}/{sid}")
                            },
                            toast
                        )
                    }
                }
            }
        ),
        _ => unreachable!()
    }

}
