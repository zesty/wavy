use dioxus::prelude::*;
use dioxus::hooks::Coroutine;

use crate::{
    channel::{queue_song, AddToQueue},
    search::{get_artist_page, AlbumDisplay, ArtistPageReq, SongDisplay},
    SERVER_DEFAULT_VALS,
    ui::asset::*
};

use super::{home::AsideProps, toast::ToastAction, load::{LoadWheel, LazyLoadImage}};

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct ArtistProps {
    pub result: Signal<Vec<AlbumDisplay>>,
    pub name: Signal<String>,
    pub last_search: Signal<Option<ArtistPageReq>>,
    pub buttons_visible: Signal<&'static str>,
    pub prev_visible: Signal<&'static str>,
    pub next_visible: Signal<&'static str>,
    pub login_panel: Signal<&'static str>,
    pub artist_page_sel: Signal<bool>,
    pub loading: Signal<bool>
}

impl ArtistProps {
    pub fn new(login_panel: Signal<&'static str>) -> Self {
        Self {
            result: use_signal(Vec::<AlbumDisplay>::new),
            name: use_signal(String::new),
            last_search: use_signal(|| None),
            buttons_visible: use_signal(|| "hide-item-n"),
            prev_visible: use_signal(|| ""),
            next_visible: use_signal(|| ""),
            login_panel,
            artist_page_sel: use_signal(|| false),
            loading: use_signal(|| false)
        }
    }
}

#[component]
pub fn ArtistPanel(a: AsideProps, r: ArtistProps, toast: Coroutine<ToastAction>) -> Element {
    rsx!(
        div {
            class: "{a.artist_hide}",
            if r.artist_page_sel.read().eq(&false) {
                if r.loading.read().eq(&false) {
                    div {
                        class: "artist-not-sel",
                        img { class: "artist-not-sel-icon", src: ARTIST_ICON }
                        h2 { class: "aside-title", "No artist selected" },
                    }
                } else {
                    LoadWheel {}
                }
            } else {
                div {
                    class: "artist-res-page",
                    h2 { class: "aside-title", "{r.name}" }
                    ul {
                        id: "artist-results",
                        class: "listing-box",
                        for n in (r.result)() {
                            AlbumView { r: n, toast: toast }
                        }
                    }
                    div {
                        class: "page-buttons {r.buttons_visible}",
                        img {
                            class: "nav-button {r.prev_visible}",
                            src: PREV_ICON,
                            onclick: move |_| {
                                let mut s_id = None;
                                if let Some(v) = r.last_search.read().clone() {
                                    if let Some(first) = r.result.read().first() {
                                        s_id = Some((v, first.id.clone()));
                                    }
                                }

                                if let Some((mut v, id)) = s_id {
                                    v.rev = Some(true);
                                    v.page = Some(id);
                                    load_artist_page(v, a, r, toast);
                                }
                            }
                        }
                        img {
                            class: "nav-button {r.next_visible}",
                            src: NEXT_ICON,
                            onclick: move |_| {
                                let mut s_id = None;
                                if let Some(v) = r.last_search.read().clone() {
                                    if let Some(last) = r.result.read().last() {
                                        s_id = Some((v, last.id.clone()));
                                    }
                                }

                                if let Some((mut v, id)) = s_id {
                                    v.rev = None;
                                    v.page = Some(id);
                                    load_artist_page(v, a, r, toast);
                                }
                            }
                        }
                    }
                }
            }
        }
    )
}

#[component]
fn AlbumView(r: AlbumDisplay, toast: Coroutine<ToastAction>) -> Element {
    let res = r.songs;
    rsx!(
        li {
            div {
                class: "playlist-element",
                LazyLoadImage { class: "aside-song-art", src: "/api/v1/songartwork/{r.id}" }
                div {
                    p {
                        class: "aside-song-title",
                        "{r.name}"
                    }
                }
            }
            ul {
                class: "songs-box",
                for n in res {
                    SongView { album_id: r.id.clone(), r: n, toast: toast }
                }
            }
        }
    )
}

#[component]
fn SongView(album_id: String, r: SongDisplay, toast: Coroutine<ToastAction>) -> Element {
    rsx!(
        li {
            class: "playable-element-artist",
            div {
                p {
                    class: "aside-song-title",
                    "{r.name}"
                }
            }
            img {
                src: ADD_TO_PL_ICON,
                onclick: move |_| {
                    queue_song(
                        AddToQueue {
                            channel_id: SERVER_DEFAULT_VALS.default_channel.clone(),
                            song_id: format!("{album_id}/{}", r.id)
                        },
                        toast
                    )
                }
            }
        }
    )
}

pub fn load_artist_page_init(artist_id: String, a: AsideProps,
                    artist_props: ArtistProps, toast: Coroutine<ToastAction>) {
    let search = ArtistPageReq {
        artist_id,
        page: None,
        page_size: 50,
        rev: None
    };
    load_artist_page(search, a, artist_props, toast);
}

pub fn load_artist_page(search: ArtistPageReq, mut a: AsideProps,
                        mut r: ArtistProps, toast: Coroutine<ToastAction>) {
    if !r.login_panel.read().eq(LOGOUT_ICON) {
        toast.send(ToastAction { message: "You need to be logged in to browse artist page".to_owned() });
        return;
    }

    show_artist_section(&mut a);
    // Skipping if we already are in same page
    if r.last_search.read().as_ref().is_some_and(|x| x.artist_id.eq(&search.artist_id) && x.page.eq(&search.page)) {
        return;
    }

    // Showing search wheel
    r.loading.set(true);
    if r.artist_page_sel.read().ne(&false) {
        r.artist_page_sel.set(false);
    }

    r.last_search.set(Some(search.clone()));

    spawn(async move {
        let first = search.page.is_none();
        let back  = search.rev.is_some_and(|r| r);
        let ret   = get_artist_page(
            search,
            &mut r,
            toast
        ).await;

        r.loading.set(false);

        if r.artist_page_sel.read().ne(&true) {
            r.artist_page_sel.set(true);
        }

        match ret {
            None => {
                if first {
                    r.buttons_visible.set("hide-item-n");
                } else {
                    toast.send(ToastAction {
                        message: if back {
                            "You are at the front page already"
                        } else {
                            "Last page already reached"
                        }.to_owned()
                    });
                    r.prev_visible.set("hide-item-n");
                }
            },
            Some(_) => {
                r.buttons_visible.set("");
                if first {
                    r.prev_visible.set("hide-item-n");
                } else {
                    r.prev_visible.set("");
                }
                if r.result.read().len() == 50 {
                    r.next_visible.set("");
                } else {
                    r.next_visible.set("hide-item-n");
                }
            }
        }
    });
}

pub fn show_artist_section(a: &mut AsideProps) {
    if a.artist_selected.read().eq("") {
        a.search_selected.set("");
        a.playlist_selected.set("");
        a.artist_selected.set("nav-dir-selected");
        a.active_stack_item.set("artist-open");
        a.playlist_hide.set("hide-item");
        a.search_hide.set("hide-item");
        a.artist_hide.set("");
    }
}
