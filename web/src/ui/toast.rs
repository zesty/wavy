use std::{future::Future, pin::Pin};

use dioxus::prelude::*;
use futures::{
    StreamExt,
    future::{select, Either, pending}
};
use gloo_timers::future::TimeoutFuture;


#[component]
pub fn Toast(style: Signal<&'static str>, msg: Signal<String>) -> Element {
    rsx! {
        div {
            class: "toast {style}",
            "{msg}"
        }
    }
}

#[derive(PartialEq)]
pub struct ToastAction {
    pub message: String
}

pub fn toast_listener(mut style: Signal<&'static str>, mut msg: Signal<String>) -> Coroutine<ToastAction> {
    use_coroutine(|mut rx: UnboundedReceiver<ToastAction>| async move {
        // Here we want to have toast show up for exactly 5 seconds.
        // If next toast wants to show up while current toast is still displaying
        // then we quickly skip it to display next toast
        let mut sleep: Pin<Box<dyn Future<Output = ()>>> = Box::pin(pending::<()>());

        loop {
            match select(rx.next(), sleep).await {
                Either::Left((v, _)) => match v {
                    Some(action) => {
                        style.set("");
                        msg.set("".to_string());
                        TimeoutFuture::new(50).await;
                        msg.set(action.message);
                        style.set("toast-show");
                        sleep = Box::pin(TimeoutFuture::new(5000));
                    },
                    None => break
                },
                Either::Right((_, _)) => {
                    style.set("");
                    msg.set("".to_string());
                    sleep = Box::pin(pending::<()>());
                }
            }
        }
    })
}
