use std::collections::VecDeque;

use dioxus::prelude::*;
use crate::{
    channel::{ChannelStateReader, Song}, ui::{
        account::Login, search::SearchPanel, toast::{toast_listener, Toast},
        artist::{ArtistProps, ArtistPanel, load_artist_page_init, show_artist_section},
        asset::*, load::LazyLoadImage
    }, SERVER_DEFAULT_VALS,
    account::{logout, check_status}
};

use super::{
    toast::ToastAction,
    player::CurrentPlaying
};

#[component]
pub fn Home() -> Element {
    let player_icon           = use_signal(|| PLAY_ICON);
    let player_paused         = use_signal(|| true);
    let current               = use_signal(Song::new);
    let current_visible_style = use_signal(|| "");
    let next                  = use_signal(VecDeque::<Song>::new);
    let side_panel_style      = use_signal(|| "ct-layout");
    let login_style           = use_signal(|| "modal-hidden");
    let login_panel           = use_signal(|| LOGIN_ICON);
    let toast_style           = use_signal(|| "");
    let toast_msg             = use_signal(|| "".to_string());
    let toast                 = toast_listener(toast_style, toast_msg);
    let _ch_state_reader      = ChannelStateReader::new(current, next, SERVER_DEFAULT_VALS.default_channel.to_string());
    let artist_props          = ArtistProps::new(login_panel);
    let aside_props           = AsideProps::new();

    let user = use_signal(|| "".to_string());
    let pass = use_signal(|| "".to_string());

    rsx! {
        link {
            rel: "shortcut icon",
            href: "/favicon.ico?={FAVICON}"
        }
        link {
            rel: "stylesheet",
            href: MAIN_STYLE
        },
        link {
            rel: "stylesheet",
            href: LOAD_STYLE
        },
        main {
            class: "main",
            div { class: "blured-bg", background: "url({current.read().artwork}) no-repeat center /cover"}
            Login {
                login_style: login_style, toast: toast,
                login_panel: login_panel,
                current_visible_style: current_visible_style,
                user: user,
                pass: pass
            }
            div {
                class: "content-container",
                TopPanel {
                    side_panel_style: side_panel_style,
                    current_visible_style: current_visible_style,
                    login_style: login_style,
                    login_panel: login_panel,
                    toast: toast,
                    user: user,
                    pass: pass
                }
                div {
                    class: "content {side_panel_style}",
                    CurrentPlaying {
                        player_icon: player_icon,
                        current: current,
                        player_paused: player_paused,
                        current_visible_style: current_visible_style,
                        a: aside_props,
                        artist_props: artist_props,
                        toast: toast
                    }
                    AsideSection {
                        next: next,
                        toast: toast,
                        a: aside_props,
                        artist_props: artist_props
                    }
                }
            }
            Toast { style: toast_style, msg: toast_msg }

            a { class: "hide-item", href: "http://www.internet-radio.com", "Internet Radio" }
            // Keep icons in dom in order not to refetch them from server
            div {
                class: "hide-item",
                img { src: "{LOGIN_ICON}" },
                img { src: "{LOGOUT_ICON}" },
                img { src: "{MENU_ICON}" },
                img { src: "{CLOSE_ICON}" },
                img { src: "{PLAY_ICON}" },
                img { src: "{STOP_ICON}" }
            }
        }
    }
}

#[component]
fn TopPanel(mut side_panel_style: Signal<&'static str>,
            mut current_visible_style: Signal<&'static str>,
            mut login_style: Signal<&'static str>,
            mut login_panel: Signal<&'static str>,
            toast: Coroutine<ToastAction>,
            mut user: Signal<String>, mut pass: Signal<String>) -> Element {
    let mut open_panel = use_signal(|| MENU_ICON);
    use_effect(move || check_status(login_panel));
    rsx!(
        div {
            id: "top-panel",
            class: "top-panel hide-item",
            img {
                class: "logo",
                src: LOGO
            }
            img {
                id: "login-button",
                class: "nav-button",
                src: "{login_panel}",
                onclick: move |_| {
                    if login_panel.read().eq(LOGOUT_ICON) {
                        logout(toast, login_panel);
                        return;
                    }

                    if !open_panel.read().eq(MENU_ICON) {
                        open_panel.set(MENU_ICON);
                        side_panel_style.set("ct-layout");
                    }

                    if login_style.read().eq("modal-visible") {
                        login_style.set("modal-hidden");
                        login_panel.set(LOGIN_ICON);
                        current_visible_style.set("");
                        user.set("".to_owned());
                        pass.set("".to_owned());
                    } else {
                        login_style.set("modal-visible");
                        login_panel.set(CLOSE_ICON);
                        current_visible_style.set("hide-item");
                    }
                }
            }
            img {
                id: "aside-panel-button",
                class: "nav-button",
                src: "{open_panel}",
                onclick: move |_| {
                    if login_style.read().eq("modal-visible") {
                        login_style.set("modal-hidden");
                        login_panel.set(LOGIN_ICON);
                    }

                    if open_panel.read().eq(MENU_ICON) {
                        open_panel.set(CLOSE_ICON);
                        side_panel_style.set("ct-layout-open");
                        current_visible_style.set("current-hide");
                    } else {
                        open_panel.set(MENU_ICON);
                        side_panel_style.set("ct-layout");
                        current_visible_style.set("");
                    }
                }
            }
        }
    )
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct AsideProps {
    pub playlist_selected: Signal<&'static str>,
    pub search_selected: Signal<&'static str>,
    pub artist_selected: Signal<&'static str>,
    pub active_stack_item: Signal<&'static str>,
    pub playlist_hide: Signal<&'static str>,
    pub search_hide: Signal<&'static str>,
    pub artist_hide: Signal<&'static str>
}

impl AsideProps {
    pub fn new() -> Self {
        Self {
            playlist_selected: use_signal(|| "nav-dir-selected"),
            search_selected: use_signal(|| ""),
            artist_selected: use_signal(|| ""),
            active_stack_item: use_signal(|| "playlist-open"),
            playlist_hide: use_signal(|| ""),
            search_hide: use_signal(|| "hide-item"),
            artist_hide: use_signal(|| "hide-item")
        }
    }
}

#[component]
fn AsideSection(next: Signal<VecDeque<Song>>, toast: Coroutine<ToastAction>,
                a: AsideProps, artist_props: ArtistProps) -> Element {
    rsx!(
        div {
            class: "aside-section",
            div {
                class: "nav-bar",
                div {
                    class: "nav-dir {a.playlist_selected}",
                    onclick: move |_| {
                        a.playlist_selected.set("nav-dir-selected");
                        a.search_selected.set("");
                        a.artist_selected.set("");
                        a.active_stack_item.set("playlist-open");
                        a.playlist_hide.set("");
                        a.search_hide.set("hide-item");
                        a.artist_hide.set("hide-item");
                    },
                    img {
                        src: PLAYLIST_ICON
                    }
                }
                div {
                    class: "nav-dir {a.search_selected}",
                    onclick: move |_| {
                        a.search_selected.set("nav-dir-selected");
                        a.playlist_selected.set("");
                        a.artist_selected.set("");
                        a.active_stack_item.set("search-open");
                        a.playlist_hide.set("hide-item");
                        a.search_hide.set("");
                        a.artist_hide.set("hide-item");
                    },
                    img {
                        src: SEARCH_ICON
                    }
                }
                div {
                    class: "nav-dir {a.artist_selected}",
                    onclick: move |_| {
                        show_artist_section(&mut a);
                    },
                    img {
                        src: ARTIST_ICON
                    }
                }
            }
            div {
                class: "aside-stack {a.active_stack_item}",
                div {
                    class: "aside-page {a.playlist_hide}",
                    h2 { class: "aside-title", "Playing next" }
                    ul {
                        class: "listing-box",
                        for n in next() {
                            li {
                                class: "playlist-element",
                                LazyLoadImage { class: "aside-song-art", src: "{n.artwork}", not_lazy: true }
                                div {
                                    p {
                                        class: "aside-song-title clickable",
                                        onclick: move |_| {
                                            load_artist_page_init(n.artist_id.clone(), a, artist_props, toast);
                                        },
                                        "{n.artist}"
                                    }
                                    p {
                                        class: "aside-song-title",
                                        "{n.name}"
                                    }
                                }
                            }
                        }
                    }
                }
                SearchPanel { search_hide: a.search_hide, toast: toast, aside_props: a, artist_props: artist_props }
                ArtistPanel { a: a, r: artist_props, toast: toast }
            }
        }
    )
}
