use dioxus::prelude::*;

#[component]
pub fn LoadWheel() -> Element {
    rsx! {
        div {
            class: "artist-not-sel",
            div {
                class: "load-wheel-container",
                div {
                    class: "load-wheel",
                    div {}
                    div {}
                }
            }
            h2 { class: "aside-title", "Loading..." },
        }
    }
}

#[derive(Props, PartialEq, Clone)]
pub struct LazyLoadImageProps {
    class: &'static str,
    src: String,
    #[props(default)]
    not_lazy: bool
}

#[component]
pub fn LazyLoadImage(props: LazyLoadImageProps) -> Element {
    let mut loaded = use_signal(|| "loader");
    rsx! {
        img {
            class: "{props.class} {loaded}",
            alt: " ",
            loading: if props.not_lazy { "eager" } else { "lazy" },
            src: "{props.src}",
            onload: move |_| {
                loaded.set("");
            }
        }
    }
}
