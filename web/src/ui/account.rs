use dioxus::prelude::*;
use crate::account::LoginCredentials;

use super::{toast::ToastAction, asset::*};

#[component]
pub fn Login(login_style: Signal<&'static str>, toast: Coroutine<ToastAction>,
             login_panel: Signal<&'static str>, current_visible_style: Signal<&'static str>,
             mut user: Signal<String>, mut pass: Signal<String>) -> Element {

    rsx! {
        div {
            class: "modal-panel {login_style}",
            h2 { class: "aside-title", "Login" }
            form {
                id: "login-form",
                class: "login-form",
                onsubmit: move |_| {
                    spawn(async move {
                        if crate::account::login(
                            LoginCredentials {
                                user: user.read().to_string(),
                                pass: pass.read().to_string()
                            },
                            toast
                        ).await.is_ok() {
                            login_panel.set(LOGOUT_ICON);
                            login_style.set("modal-hidden");
                            current_visible_style.set("");
                            user.set("".to_owned());
                            pass.set("".to_owned());
                        }
                    });
                },
                input {
                    class: "in",
                    name: "user",
                    placeholder: "Username",
                    "type": "text",
                    required: "true",
                    value: "{user}",
                    oninput: move |event| user.set(event.value())
                }
                input {
                    class: "in",
                    name: "pass",
                    placeholder: "Password",
                    "type": "password",
                    required: "true",
                    value: "{pass}",
                    oninput: move |event| pass.set(event.value())
                }
                button {
                    class: "in",
                    "type": "submit",
                    "Login"
                }
            }
        }
    }
}
