use wavy_web::asset;

pub static LOGIN_ICON: &str     = asset!("login.svg");
pub static LOGOUT_ICON: &str    = asset!("logout.svg");
pub static ARTIST_ICON: &str    = asset!("artist.svg");
pub static PREV_ICON: &str      = asset!("prev.svg");
pub static NEXT_ICON: &str      = asset!("next.svg");
pub static ADD_TO_PL_ICON: &str = asset!("add_to_pl.svg");
pub static PLAY_ICON: &str      = asset!("play.svg");
pub static STOP_ICON: &str      = asset!("stop.svg");
pub static MENU_ICON: &str      = asset!("menu.svg");
pub static CLOSE_ICON: &str     = asset!("close.svg");
pub static PLAYLIST_ICON: &str  = asset!("playlist.svg");
pub static SEARCH_ICON: &str    = asset!("search.svg");
pub static DOWN_ICON: &str      = asset!("down.svg");

pub static MAIN_STYLE: &str     = asset!("main.css");
pub static LOAD_STYLE: &str     = asset!("load.css");

pub static LOGO: &str           = asset!("logo.png");
pub static FAVICON: &str        = asset!("favicon.ico");

pub static SILENCE_AUDIO: &str  = asset!("silence.mp3");
