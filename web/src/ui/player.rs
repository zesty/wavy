use std::sync::OnceLock;

use dioxus::prelude::*;
use web_sys::{wasm_bindgen::{prelude::Closure, JsCast}, HtmlImageElement, MediaSessionAction, MediaSessionPlaybackState};

use crate::{
    channel::{Song, ChannelStateReader},
    ui::{artist::load_artist_page_init, asset::*}, SERVER_DEFAULT_VALS
};

use super::{artist::ArtistProps, home::AsideProps, toast::ToastAction};

pub struct MediaSessionCallbacks {
    play: Closure<dyn FnMut()>,
    pause: Closure<dyn FnMut()>,
    stop: Closure<dyn FnMut()>
}

unsafe impl Send for MediaSessionCallbacks {}
unsafe impl Sync for MediaSessionCallbacks {}

#[component]
pub fn CurrentPlaying(mut player_icon: Signal<&'static str>, current: Signal<Song>,
                      player_paused: Signal<bool>, current_visible_style: Signal<&'static str>,
                      a: AsideProps, artist_props: ArtistProps, toast: Coroutine<ToastAction>) -> Element {
    let mut volume   = use_signal(|| "1".to_string());
    let window       = web_sys::window().unwrap();
    let mediasession = window.navigator().media_session();

    use_effect(move || {
        if let Some(Some(storage)) = web_sys::window().and_then(|x| x.local_storage().ok()) {
            if let Ok(Some(vol)) = storage.get("volume") {
                set_volume(&mut volume, vol);
            }
        }
    });

    // Making sure callbacks are never dropped
    // We never should ever access them so it is safe
    static CALLBACKS: OnceLock<MediaSessionCallbacks> = OnceLock::new();
    CALLBACKS.get_or_init(|| {
        // Adding action handlers for media access api
        let play_closure = Closure::<dyn FnMut()>::new(move || {
            click_play_button();
        });
        let pause_closure = Closure::<dyn FnMut()>::new(move || {
            click_play_button();
        });
        let stop_closure = Closure::<dyn FnMut()>::new(move || {
            click_play_button();
        });
        
        mediasession.set_action_handler(
            MediaSessionAction::Play,
            Some(play_closure.as_ref().unchecked_ref())
        );
        mediasession.set_action_handler(
            MediaSessionAction::Pause,
            Some(pause_closure.as_ref().unchecked_ref())
        );
        mediasession.set_action_handler(
            MediaSessionAction::Stop,
            Some(stop_closure.as_ref().unchecked_ref())
        );

        MediaSessionCallbacks {
            play: play_closure,
            pause: pause_closure,
            stop: stop_closure
        }
    });

    let space_key = Key::Character(" ".to_string());

    rsx!(
        div {
            class: "current-container",
            onkeydown: move |event: KeyboardEvent| {
                if event.key().eq(&space_key) {
                    click_play_button();
                }
            },
            tabindex: "0",
            div {
                class: "current {current_visible_style}",
                img {
                    class: "song-art",
                    alt: " ",
                    src: "{current.read().artwork}"
                }
                p {
                    class: "song-title",
                    span {
                        class: "clickable",
                        onclick: move |_| {
                            if let Some(el) = web_sys::window().unwrap().document().unwrap().get_element_by_id("aside-panel-button") {
                                let el: HtmlImageElement = el.dyn_into().unwrap();
                                if !el.src().ends_with(CLOSE_ICON) {
                                    el.click();
                                }
                            }
                            load_artist_page_init(current.read().artist_id.clone(), a, artist_props, toast);
                        },
                        "{current.read().artist}"
                    }
                    span {
                        " - "
                    }
                    span {
                        "{current.read().name}"
                    }
                }
                audio {
                    id: "player",
                    preload: "none",
                    onended: move |_| {
                        player_icon.set(PLAY_ICON);
                        player_paused.set(true);
                    }
                }
                audio { id: "silence", preload: "auto", "loop": "true", src: SILENCE_AUDIO }
                input {
                    "type": "range",
                    value: "{volume}",
                    min: "0",
                    max: "1",
                    step: "0.01",
                    oninput: move |vol| {
                        set_volume(&mut volume, vol.value());
                    }
                }
                img {
                    id: "play_button",
                    src: "{player_icon}",
                    onclick: move |_| {
                        let window   = web_sys::window().unwrap();
                        let document = window.document().unwrap();
                        let player   = document.get_element_by_id("player").unwrap();
                        let player   = player.dyn_into::<web_sys::HtmlAudioElement>()
                            .map_err(|_| ())
                            .unwrap();
                        let silence  = document.get_element_by_id("silence").unwrap();
                        let silence  = silence.dyn_into::<web_sys::HtmlAudioElement>()
                            .map_err(|_| ())
                            .unwrap();
                        let mediasession = window.navigator().media_session();

                        if player_paused.read().eq(&true) {
                            player.set_src(&format!("{}radio/{}.mp3", SERVER_DEFAULT_VALS.path, SERVER_DEFAULT_VALS.default_channel));
                            _ = silence.pause();
                            _ = player.play();
                            player_icon.set(STOP_ICON);
                            player_paused.set(false);
                            mediasession.set_playback_state(MediaSessionPlaybackState::Playing);
                        } else {
                            _ = silence.play();
                            _ = player.pause();
                            player.set_src("");
                            player_icon.set(PLAY_ICON);
                            player_paused.set(true);
                            _ = silence.pause();
                            mediasession.set_playback_state(MediaSessionPlaybackState::Paused);
                        }
                    }
                }
            }
        }
    )
}

fn click_play_button() {
    let window = web_sys::window().unwrap();
    let player = window.document().unwrap().get_element_by_id("play_button").unwrap();
    let play   = player.dyn_into::<web_sys::HtmlImageElement>()
        .map_err(|_| ())
        .unwrap();
    play.click();
}

fn set_volume(volume: &mut Signal<String>, vol: String) {
    if let Ok(v) = vol.parse::<f64>() {
        let window   = web_sys::window().unwrap();
        let document = window.document().unwrap();
        let player   = document.get_element_by_id("player").unwrap();
        let player   = player.dyn_into::<web_sys::HtmlAudioElement>()
            .map_err(|_| ())
            .unwrap();
        if let Some(Some(storage)) = web_sys::window().and_then(|x| x.local_storage().ok()) {
            _ = storage.set("volume", &vol);
        }
        volume.set(vol);
        player.set_volume(v);
    }
}
