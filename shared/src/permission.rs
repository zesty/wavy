
/// Here we are managing permissions given to users by using bit operation
/// 4 left bits are not used
/// Two first bit from right (000000BB):
///     - 00: 5 playlists maximum
///     - 01: 10 playlists maximum
///     - 10: 20 playlists maximum
///     - 11: infinite playlists
/// Two bits from right in third and fourth positions (0000BB00):
///     - 00: No ability to create/delete radio channels.
///     - 01: 2 maximum radio channels
///     - 10: 5 maximum radio channels
///     - 11: Infinite creation of channels + managing all other channels
#[derive(Default, Debug, PartialEq, PartialOrd, Clone, Copy)]
pub struct PermissionLevel {
    perms: u8
}

impl std::convert::From<u8> for PermissionLevel {
    fn from(value: u8) -> Self {
        read_permission(value)
    }
}

pub fn read_permission(perm: u8) -> PermissionLevel {
    PermissionLevel {
        perms: perm
    }
}

impl PermissionLevel {
    /// Creating a permission with maximum access
    pub fn new_admin() -> Self {
        Self {
            perms: 255
        }
    }

    pub fn as_num(&self) -> u8 {
        self.perms
    }

    /// Max number of playlist than can be created
    pub fn playlists_max(&self) -> usize {
        if self.perms & 0b00000011 == 3 {
            usize::MAX
        } else if self.perms & 0b00000001 > 0 {
            10
        } else if self.perms & 0b00000010 > 0 {
            20
        } else {
            5
        }
    }

    pub fn can_create_channels(&self) -> bool {
        self.perms & 0b00001000 > 0 || self.perms & 0b00000100 > 0
    }

    pub fn channels_max(&self) -> usize {
        if self.perms & 0b00001100 == 12 {
            usize::MAX
        } else if self.perms & 0b00000100 > 0 {
            2
        } else if self.perms & 0b00001000 > 0 {
            5
        } else {
            0
        }
    }

    /// Check if user can manage other channels
    pub fn is_channels_manager(&self) -> bool {
        self.perms & 0b00001100 == 12
    }
}
