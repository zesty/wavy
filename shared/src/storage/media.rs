use async_stream::stream;
use futures::{Stream, StreamExt};
use sqlx::{
    error::ErrorKind, sqlite::{Sqlite, SqliteConnectOptions, SqlitePool},
    Error, FromRow, Pool, Transaction
};
use uuid::Uuid;
use std::{path::PathBuf, str::FromStr, pin::Pin};
use anyhow::Result;
use rand::Rng;
use serde::{Serialize, Deserialize};

use super::{SchemaVersion, TableCount, TableHasRow};

#[derive(FromRow)]
pub struct AuthorId {
    pub id: Uuid
}

#[derive(Debug, FromRow)]
pub struct SongRequestPicker {
    pub album: Uuid,
    pub id: u32,
    pub name: String,
    pub artist: String,
    pub artist_id: Uuid
}

pub enum AddAlbumStatus {
    Ok,
    Exists
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Album {
    pub name: String,
    pub date: Option<String>,
    pub author: String,
    pub author_id: Option<Uuid>,
    pub author_country_code: Option<String>,
    pub tracks: Vec<Track>,
    pub artwork: Vec<PathBuf>
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Track {
    pub name: String,
    pub track_number: u32,
    pub duration: i64,
    pub location: PathBuf,
    pub codec: String,
    pub bitrate: Option<usize>,
    pub sample_rate: usize,
    pub disc: Option<u32>
}

#[derive(Debug, Serialize)]
pub struct AlbumDisplay {
    pub name: String,
    pub id: uuid::Uuid,
    pub songs: Vec<SongDisplay>
}

#[derive(Debug, Serialize, FromRow)]
pub struct SongDisplay {
    pub id: u32,
    pub name: String,
    pub duration: i64
}

#[derive(Debug, FromRow)]
pub struct FetchAlbumDisplay {
    pub name: String,
    pub id: uuid::Uuid
}

#[derive(Debug, FromRow)]
pub struct ArtistInfo {
    pub name: String,
    pub album_count: u32
}

#[derive(Debug, FromRow, Serialize)]
pub struct SearchArtist {
    pub artist_name: String,
    pub artist_id: uuid::Uuid
}

#[derive(Debug, FromRow, Serialize)]
pub struct SearchAlbum {
    pub artist_name: String,
    pub artist_id: uuid::Uuid,
    pub album_name: String,
    pub album_id: uuid::Uuid
}

#[derive(Debug, FromRow, Serialize)]
pub struct SearchSong {
    pub artist_name: String,
    pub artist_id: uuid::Uuid,
    pub album_name: String,
    pub album_id: uuid::Uuid,
    pub song_name: String,
    pub song_id: u32
}

#[derive(Debug, FromRow, Serialize)]
pub struct SearchSong2 {
    pub artist_name: String,
    pub artist_id: uuid::Uuid,
    pub album_id: uuid::Uuid,
    pub song_name: String,
    pub song_id: u32
}

#[derive(Debug, FromRow, Serialize)]
pub struct PlaylistSong {
    pub artist_name: String,
    pub artist_id: uuid::Uuid,
    pub album_id: uuid::Uuid,
    pub song_name: String,
    pub song_id: u32,
    pub pos: u32
}

#[derive(FromRow)]
pub struct PendingAlbum {
    pub id: Uuid
}

#[derive(Debug, Clone)]
pub struct MediaInfoStorage {
    pub con: Pool<Sqlite>
}

impl MediaInfoStorage {
    pub async fn new(path: &str) -> Result<MediaInfoStorage> {
        let db = MediaInfoStorage { con: MediaInfoStorage::connect(path).await? };

        let version = sqlx::query_as::<_, SchemaVersion>("pragma user_version")
            .fetch_one(&db.con)
            .await?;

        if version.user_version <= 0 {
            db.create_if_not_exists().await?;
        }
        if version.user_version < 2 {
            db.migrate_v2().await?;
        }
        Ok(db)
    }

    async fn connect(path: &str) -> Result<Pool<Sqlite>> {
        let opt = SqliteConnectOptions::from_str(&format!("sqlite://{}", path))?
            .create_if_missing(true);

        Ok(SqlitePool::connect_with(opt).await?)
    }

    async fn create_if_not_exists(&self) -> Result<()> {
        sqlx::query(r#"
create table if not exists author (
    id text primary key,
    name text not null
) without rowid;
create table if not exists album (
    id text primary key,
    name text not null,
    author text not null,
    date text,
    foreign key(author) references author(id) on delete cascade
) without rowid;
create table if not exists song (
    album text,
    id integer,
    name text not null,
    duration integer not null,
    primary key(album, id),
    foreign key(album) references album(id) on delete cascade
) without rowid;
create table if not exists request_metrics (
    album text,
    song integer,
    request_times integer not null,
    last_requested integer not null,
    primary key(album, song),
    foreign key(song, album) references song(id, album) on delete cascade
);
create table if not exists pending_album (
    id text primary key
) without rowid;

pragma journal_mode = WAL;
pragma user_version = 1;
            "#)
            .execute(&self.con)
            .await?;

        Ok(())
    }

    async fn migrate_v2(&self) -> Result<()> {
        sqlx::query(r#"
create table if not exists country (
    id integer primary key,
    code text
);

alter table author
    add column country integer
    references country(id);

pragma user_version = 2;
            "#)
            .execute(&self.con)
            .await?;

        Ok(())
    }
    

    pub async fn add_pending_album(&self, album_id: &Uuid) -> Result<()> {
        sqlx::query(r#"insert into pending_album values (?1)"#)
            .bind(album_id)
            .execute(&self.con)
            .await?;
        Ok(())
    }


    // TODO: Can't find a consistent way to abstract transaction to simply executor
    pub async fn remove_pending_album(&self, album_id: &Uuid) -> Result<()> {
        sqlx::query(r#"delete from pending_album where id = ?1"#)
            .bind(album_id)
            .execute(&self.con)
            .await?;

        Ok(())
    }
    async fn remove_pending_album_inner(&self, ex: &mut Transaction<'static, Sqlite>, album_id: &Uuid) -> Result<()> {
        sqlx::query(r#"delete from pending_album where id = ?1"#)
            .bind(album_id)
            .execute(&mut **ex)
            .await?;

        Ok(())
    }

    pub async fn pending_albums<'a>(&'a self) -> Pin<Box<dyn futures::stream::Stream<Item = Result<PendingAlbum, sqlx::Error>> + Send + 'a>> {
        sqlx::query_as::<_, PendingAlbum>(r#"select id from pending_album"#)
            .fetch(&self.con)
    }

    pub async fn add_album(&self, album_id: &Uuid, album: &Album) -> Result<AddAlbumStatus> {
        // We create a transaction where we insert album followed by all of it's tracks
        // We also add author if it's not there yet
        let mut tx = self.con.begin().await?;

        // First we need to check author id
        let author_id = match album.author_id {
            Some(v) => v,
            None => {
                let ret = sqlx::query_as::<_, AuthorId>(r#"select id from author where lower(name) like lower(?1)"#)
                    .bind(&album.author)
                    .fetch_one(&mut *tx)
                    .await;

                match ret {
                    Ok(v) => v.id,
                    Err(Error::RowNotFound) => Self::add_author_inner(&mut tx, &album.author, &album.author_country_code).await?,
                    Err(e) => return Err(e.into())
                }
            }
        };

        let exists = sqlx::query_as::<_, TableHasRow>(r#"select exists(select 1 from album where lower(name) like lower(?1) and author = ?2) as "exists""#)
            .bind(&album.name)
            .bind(author_id)
            .fetch_one(&mut *tx)
            .await?
            .exists;

        if exists {
            return Ok(AddAlbumStatus::Exists);
        }

        sqlx::query(r#"insert into album values (?1, ?2, ?3, ?4)"#)
            .bind(album_id)
            .bind(&album.name)
            .bind(author_id)
            .bind(&album.date)
            .execute(&mut *tx)
            .await?;

        for track in &album.tracks {
            sqlx::query(r#"insert into song values (?1, ?2, ?3, ?4)"#)
                .bind(album_id)
                .bind(track.track_number)
                .bind(&track.name)
                .bind(track.duration)
                .execute(&mut *tx)
                .await?;
        }

        self.remove_pending_album_inner(&mut tx, album_id).await?;

        tx.commit().await?;
        Ok(AddAlbumStatus::Ok)
    }

    async fn add_author_inner(ex: &mut Transaction<'static, Sqlite>, name: &str, country_code: &Option<String>) -> Result<Uuid> {
        let mut seed: rand::rngs::StdRng = rand::SeedableRng::from_entropy();
        let mut author_id;
        for i in 0..3 {
            author_id = uuid::Uuid::from_u128(seed.gen::<u128>());
            let ret   = sqlx::query(r#"insert into author values (?1, ?2, (select id from country where code = ?3))"#)
                .bind(author_id)
                .bind(name)
                .bind(country_code)
                .execute(&mut **ex)
                .await;

            match ret {
                Ok(_) => return Ok(author_id),
                Err(Error::Database(e)) => if e.kind() == ErrorKind::UniqueViolation && i != 2 {
                    continue;
                } else {
                    return Err(e.into())
                },
                Err(e) => return Err(e.into())
            }
        }
        // We do prevent loop from exiting before
        unreachable!()
    }

    pub async fn random_song(&self, size: u32) -> Result<Vec<SongRequestPicker>> {
        Ok(sqlx::query_as::<_, SongRequestPicker>(r#"select song.album, song.id, song.name, author.name as "artist", author.id as "artist_id"
from song, album, author where song.album = album.id and album.author = author.id order by random() limit ?1"#)
            .bind(size)
            .fetch_all(&self.con)
            .await?)
    }

    pub async fn request_song(&self, album_id: &Uuid, song_id: u32) -> Result<Option<SongRequestPicker>> {
        match sqlx::query_as::<_, SongRequestPicker>(r#"select song.album, song.id, song.name, author.name as "artist", author.id as "artist_id"
from song, album, author where song.album = ?1 and song.id = ?2 and song.album = album.id and album.author = author.id"#)
            .bind(album_id)
            .bind(song_id)
            .fetch_one(&self.con)
            .await {
            Ok(v) => Ok(Some(v)),
            Err(Error::RowNotFound) => Ok(None),
            Err(e) => Err(e.into())
        }
    }

    pub async fn get_artist_info(&self, artist_id: &Uuid) -> Result<ArtistInfo> {
        Ok(sqlx::query_as::<_, ArtistInfo>(r#"select count(*) as "album_count", author.name from album, author where album.author = author.id and author = ?1"#)
            .bind(artist_id)
            .fetch_one(&self.con)
            .await?)
    }

    pub async fn get_artist_works(&self, artist_id: &Uuid, page: &uuid::Uuid, page_size: u32, rev: bool) -> Result<Vec<AlbumDisplay>> {
        let q = format!(r#"select id, name from album where author = ?1
and id {} ?2
order by id {}
limit ?3"#,
            if rev { "<" } else { ">" },
            if rev { "desc" } else { "asc" }
        );

        let albums = sqlx::query_as::<_, FetchAlbumDisplay>(&q)
            .bind(artist_id)
            .bind(page)
            .bind(page_size)
            .fetch_all(&self.con)
            .await?;

        let mut result = Vec::new();

        for album in albums {
            let songs = sqlx::query_as::<_, SongDisplay>(r"select id, name, duration from song where album = ?1 order by id")
                .bind(album.id)
                .fetch_all(&self.con)
                .await?;

            result.push(AlbumDisplay {
                name: album.name,
                id: album.id,
                songs
            });
        }

        Ok(result)
    }

    pub async fn search_by_artist(&self, query: &str, page: &uuid::Uuid, page_size: u32, rev: bool) -> Result<Vec<SearchArtist>> {
        let q = format!(r#"select id as "artist_id", name as "artist_name"
from author where name like '%' || ?1 || '%'
and id {} ?2
order by id {}
limit ?3"#,
            if rev { "<" } else { ">" },
            if rev { "desc" } else { "asc" }
        );

        let r = sqlx::query_as::<_, SearchArtist>(&q)
            .bind(query)
            .bind(page)
            .bind(page_size)
            .fetch_all(&self.con)
            .await;

        match r {
            Ok(v) => Ok(v),
            Err(Error::RowNotFound) => Ok(Vec::new()),
            Err(e) => Err(e.into())
        }
    }

    pub async fn search_by_album(&self, query: &str, page: &uuid::Uuid, page_size: u32, rev: bool) -> Result<Vec<SearchAlbum>> {
        let q = format!(r#"select album.id as "album_id", album.name as "album_name",
author.id as "artist_id", author.name as "artist_name"
from album, author where album.author = author.id and album.name like '%' || ?1 || '%'
and album.id {} ?2
order by album.id {}
limit ?3"#,
            if rev { "<" } else { ">" },
            if rev { "desc" } else { "asc" }
        );

        let r = sqlx::query_as::<_, SearchAlbum>(&q)
            .bind(query)
            .bind(page)
            .bind(page_size)
            .fetch_all(&self.con)
            .await;

        match r {
            Ok(v) => Ok(v),
            Err(Error::RowNotFound) => Ok(Vec::new()),
            Err(e) => Err(e.into())
        }
    }

    pub async fn search_by_song(&self, query: &str, album: &uuid::Uuid, song: u32, page_size: u32, rev: bool) -> Result<Vec<SearchSong>> {
        let q = format!(r#"select song.id as "song_id", song.name as "song_name",
album.id as "album_id", album.name as "album_name",
author.id as "artist_id", author.name as "artist_name"
from song, album, author
where song.album = album.id and album.author = author.id and song.name like '%' || ?1 || '%'
and album.id {} ?2 and (album.id != ?2 or song.id {} ?3)
order by album.id {}, song.id {}
limit ?4"#,
            if rev { "<=" } else { ">=" },
            if rev { "<" } else { ">" },
            if rev { "desc" } else { "asc" },
            if rev { "desc" } else { "asc" }
        );

        let r = sqlx::query_as::<_, SearchSong>(&q)
            .bind(query)
            .bind(album)
            .bind(song)
            .bind(page_size)
            .fetch_all(&self.con)
            .await;

        match r {
            Ok(v) => Ok(v),
            Err(Error::RowNotFound) => Ok(Vec::new()),
            Err(e) => Err(e.into())
        }
    }

    pub async fn find_songs(&'_ self, mut songs: Vec<super::access::PlaylistSong>) -> impl Stream<Item = Result<PlaylistSong>> + '_ + Unpin {
        Box::pin(stream! {
            let params = format!("(?, ?){}", ", (?, ?)".repeat(songs.len() - 1));
            let q      = format!(r#"select song.id as "song_id", song.name as "song_name",
album.id as "album_id", author.id as "artist_id", author.name as "artist_name"
from song, album, author
where song.album = album.id and album.author = author.id and (album.id, song.id) in ({})"#,
                params
            );
            let mut r = sqlx::query_as::<_, SearchSong2>(&q);

            for song in &songs {
                let mut split = song.song.split('/');
                r = r.bind(Uuid::from_str(split.next().unwrap())?)
                     .bind(u32::from_str(split.next().unwrap())?);
            }

            let mut stream = r.fetch(&self.con);
            while let Some(Ok(s)) = stream.next().await {
                for i in 0..songs.len() {
                    if songs[i].song.eq(&format!("{}/{}", s.album_id, s.song_id)) {
                        let song = songs.remove(i);
                        yield Ok(PlaylistSong {
                            artist_id: s.artist_id,
                            artist_name: s.artist_name,
                            album_id: s.album_id,
                            song_name: s.song_name,
                            song_id: s.song_id,
                            pos: song.pos
                        });
                        break;
                    }
                }
            }
        })
    }

    pub async fn song_exists(&self, album_id: &Uuid, song_id: u32) -> Result<bool> {
        Ok(sqlx::query_as::<_, TableCount>(r#"select count(*) as "count" from song where album = ?1 and id = ?2"#)
            .bind(album_id)
            .bind(song_id)
            .fetch_one(&self.con)
            .await?
            .count > 0)
    }
}
