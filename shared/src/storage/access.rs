use rand::Rng;
use serde::Serialize;
use sqlx::{
    sqlite::{Sqlite, SqliteConnectOptions, SqlitePool}, types::Uuid, FromRow, Pool
};
use std::{str::FromStr, pin::Pin};
use anyhow::Result;

use crate::permission::PermissionLevel;

use super::{RowId, RowIdu32, SchemaVersion, TableCount, TableHasRow};

#[derive(FromRow)]
pub struct UserAuth {
    pub id: Uuid,
    #[sqlx(try_from = "u8")]
    pub permission: PermissionLevel
}

#[derive(FromRow)]
pub struct UserLogin {
    pub id: uuid::Uuid,
    pub pass: String
}

impl std::fmt::Display for UserAuth {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "user:[id:{}]", self.id)
    }
}

#[derive(FromRow)]
pub struct ChannelProperties {
    pub play_next_empty_queue: String
}

#[derive(FromRow)]
pub struct ChannelRequest {
    pub id: i64,
    pub song: String,
    pub play_progress: i64,
    pub title_metadata: String,
    pub artist: String,
    pub artist_id: Uuid
}

#[derive(FromRow)]
pub struct ChannelLastRequest {
    pub id: Uuid,
    pub request_id: i64,
}

#[derive(FromRow)]
pub struct TaskChannel {
    pub id: Uuid
}

#[derive(Debug, FromRow, Serialize)]
pub struct Playlist {
    pub name: String,
    pub id: Uuid
}

#[derive(Debug, FromRow)]
pub struct PlaylistSong {
    pub song: String,
    pub pos: u32
}

pub struct AddChannel {
    pub name: String,
    pub description: String,
}

#[derive(Debug, Clone)]
pub struct AccessStorage {
    pub con: Pool<Sqlite>
}

impl AccessStorage {
    pub async fn new(path: &str) -> Result<AccessStorage> {
        let db = AccessStorage { con: AccessStorage::connect(path).await? };

        let version = sqlx::query_as::<_, SchemaVersion>("pragma user_version")
            .fetch_one(&db.con)
            .await?;
        
        if version.user_version <= 0 {
            db.create_if_not_exists().await?;
        }
        if version.user_version < 2 {
            db.migrate_v2().await?;
        }
        if version.user_version < 3 {
            db.migrate_v3().await?;
        }
        if version.user_version < 4 {
            db.migrate_v4().await?;
        }
        Ok(db)
    }

    async fn connect(path: &str) -> Result<Pool<Sqlite>> {
        let opt = SqliteConnectOptions::from_str(&format!("sqlite://{}", path))?
            .create_if_missing(true);

        Ok(SqlitePool::connect_with(opt).await?)
    }

    async fn create_if_not_exists(&self) -> Result<()> {
        sqlx::query(r#"
create table if not exists user (
    id text primary key,
    name text not null,
    pass blob,
    permission integer not null,
    creation_time integer not null,
    public_requests bool not null,
    last_seen integer not null
) without rowid;
create table if not exists token (
    user text primary key,
    token blob not null,
    foreign key(user) references user(id) on delete cascade
) without rowid;
create table if not exists cookie (
    user text primary key,
    cookie text not null,
    gen_time integer not null,
    foreign key(user) references user(id) on delete cascade
) without rowid;
create table if not exists channel (
    id text primary key,
    name text not null,
    description text not null,
    play_next_empty_queue text not null,
    user text not null,
    node text not null,
    foreign key(user) references user(id) on delete cascade
) without rowid;
create table if not exists request (
    id integer primary key,
    channel text not null,
    user text,
    song text not null,
    title_metadata text not null,
    play_progress integer not null,
    foreign key(channel) references channel(id) on delete cascade,
    foreign key(user) references user(id) on delete cascade
);

pragma journal_mode = WAL;
pragma user_version = 1;
                "#)
            .execute(&self.con)
            .await?;

        Ok(())
    }

    async fn migrate_v2(&self) -> Result<()> {
        sqlx::query(r#"
create unique index idx_unique_username on user(name);

drop table cookie;

create table if not exists cookie (
    user text primary key,
    cookie blob not null,
    gen_time integer not null,
    foreign key(user) references user(id) on delete cascade
) without rowid;

pragma user_version = 2;
            "#)
            .execute(&self.con)
            .await?;

        Ok(())
    }

    async fn migrate_v3(&self) -> Result<()> {
        sqlx::query(r#"
alter table request add column artist text not null default '';
alter table request add column artist_id text not null default '';

pragma user_version = 3;
            "#)
            .execute(&self.con)
            .await?;

        Ok(())
    }

    async fn migrate_v4(&self) -> Result<()> {
        sqlx::query(r#"
create table if not exists playlist (
    user text,
    id text,
    name text not null,
    create_time integer not null,
    primary key (user, id),
    foreign key(user) references user(id) on delete cascade
) without rowid;

create table if not exists playlist_tracks (
    user text,
    playlist text,
    song text,
    pos integer not null,
    primary key (user, playlist, song),
    foreign key(user, playlist) references playlist(user, id) on delete cascade
) without rowid;
            "#)
            .execute(&self.con)
            .await?;

        Ok(())
    }

    pub async fn add_user(&self, name: &str, pass: &str, permission: PermissionLevel) -> Result<Uuid> {
        let mut seed: rand::rngs::StdRng = rand::SeedableRng::from_entropy();
        let id                           = Uuid::from_u128(seed.gen::<u128>());
        let time                         = chrono::offset::Utc::now().timestamp();
        
        sqlx::query(r#"insert into user values (?1, ?2, ?3, ?4, ?5, ?6, 0)"#)
            .bind(id)
            .bind(name)
            .bind(pass)
            .bind(permission.as_num())
            .bind(time)
            .bind(false)
            .execute(&self.con)
            .await?;

        Ok(id)
    }

    pub async fn find_user_by_name(&self, user: &str) -> Result<UserLogin> {
        Ok(sqlx::query_as::<_, UserLogin>(r#"select id, pass from user where name = ?1"#)
            .bind(user)
            .fetch_one(&self.con)
            .await?)
    }

    pub async fn update_user_token(&self, user: &Uuid, token: &[u8]) -> Result<()> {
        sqlx::query(r#"insert into token values (?1, ?2) on conflict(user) do update set token = ?2"#)
            .bind(user)
            .bind(token)
            .execute(&self.con)
            .await?;

        Ok(())
    }

    pub async fn update_user_cookie(&self, user: &Uuid, cookie: &[u8]) -> Result<()> {
        let time = chrono::offset::Utc::now().timestamp();

        sqlx::query(r#"insert into cookie values (?1, ?2, ?3) on conflict(user) do update set cookie = ?2, gen_time = ?3"#)
            .bind(user)
            .bind(cookie)
            .bind(time)
            .execute(&self.con)
            .await?;

        Ok(())
    }

    pub async fn delete_user_cookie(&self, user: &Uuid) -> Result<()> {
        sqlx::query(r#"delete from cookie where user = ?1"#)
            .bind(user)
            .execute(&self.con)
            .await?;

        Ok(())
    }

    pub async fn find_user_by_cookie(&self, cookie: &[u8]) -> Result<UserAuth> {
        Ok(sqlx::query_as::<_, UserAuth>(r#"select user.id, permission from user, cookie where user.id = cookie.user and cookie = ?1"#)
            .bind(cookie)
            .fetch_one(&self.con)
            .await?)
    }

    pub async fn find_user_by_token(&self, token: &[u8]) -> Result<UserAuth> {
        Ok(sqlx::query_as::<_, UserAuth>(r#"select user.id, permission from user, token where user.id = token.user and token = ?1"#)
            .bind(token)
            .fetch_one(&self.con)
            .await?)
    }

    pub async fn user_owned_channels_count(&self, user: &Uuid) -> Result<usize> {
        Ok(sqlx::query_as::<_, TableCount>(r#"select count(*) as "count" from channel where user = ?1"#)
            .bind(user)
            .fetch_one(&self.con)
            .await?
            .count as usize)
    }

    pub async fn add_channel(&self, channel: &AddChannel, user: &Uuid, node_id: &str) -> Result<Uuid> {
        let mut seed: rand::rngs::StdRng = rand::SeedableRng::from_entropy();
        let ch_id                        = Uuid::from_u128(seed.gen::<u128>());

        sqlx::query(r#"insert into channel values (?1, ?2, ?3, ?4, ?5, ?6)"#)
            .bind(ch_id)
            .bind(&channel.name)
            .bind(&channel.description)
            .bind("")
            .bind(user)
            .bind(node_id)
            .execute(&self.con)
            .await?;
        Ok(ch_id)
    }

    pub async fn remove_channel(&self, ch_id: &Uuid) -> Result<()> {
        if sqlx::query(r#"delete from channel where id = ?1"#)
            .bind(ch_id)
            .execute(&self.con)
            .await?
            .rows_affected() == 0 {
            Err(anyhow::Error::msg("Channel doesn't exist"))
        } else {
            Ok(())
        }
    }

    pub async fn user_owns_channel(&self, user_id: &Uuid, ch_id: &Uuid) -> Result<bool> {
        Ok(sqlx::query_as::<_, TableHasRow>(r#"select exists(select 1 from channel where id = ?1 and user = ?2) as "exists""#)
            .bind(ch_id)
            .bind(user_id)
            .fetch_one(&self.con)
            .await?
            .exists)
    }

    pub async fn channel_properties(&self, ch_id: &Uuid) -> Result<ChannelProperties> {
        Ok(sqlx::query_as::<_, ChannelProperties>(r#"select play_next_empty_queue from channel where id = ?1"#)
            .bind(ch_id)
            .fetch_one(&self.con)
            .await?)
    }

    pub async fn request_queue(&self, ch_id: &Uuid, min: i64) -> Result<Vec<ChannelRequest>> {
        Ok(sqlx::query_as::<_, ChannelRequest>(r#"select id, song, title_metadata, artist, artist_id, play_progress from request where id > ?1 and channel = ?2 order by id"#)
            .bind(min)
            .bind(ch_id)
            .fetch_all(&self.con)
            .await?)
    }

    pub async fn remove_finished_request(&self, request_id: i64) -> Result<()> {
        if sqlx::query(r#"delete from request where id = ?1"#)
            .bind(request_id)
            .execute(&self.con)
            .await?
            .rows_affected() == 0 {
            Err(anyhow::Error::msg("Can't remove last played song"))
        } else {
            Ok(())
        }
    }

    pub async fn save_playback_progress(&self, request_id: i64, play_progress: i64) -> Result<()> {
        if sqlx::query(r#"update request set play_progress = ?1 where id = ?2"#)
            .bind(play_progress)
            .bind(request_id)
            .execute(&self.con)
            .await?
            .rows_affected() == 0 {
            Err(anyhow::Error::msg("Can't remove last played song"))
        } else {
            Ok(())
        }
    }

    pub async fn channel_add_song_to_queue(&self, ch_id: &Uuid, song: &str,
                                           title_metadata: &str,
                                           artist: &str,
                                           artist_id: &Uuid,
                                           user_id: Option<&Uuid>) -> Result<i64> {
        Ok(sqlx::query_as::<_, RowId>(r#"insert into request (channel, user, song, title_metadata, artist, artist_id, play_progress) values (?1, ?2, ?3, ?4, ?5, ?6, ?7) returning id"#)
            .bind(ch_id)
            .bind(user_id)
            .bind(song)
            .bind(title_metadata)
            .bind(artist)
            .bind(artist_id)
            .bind(0)
            .fetch_one(&self.con)
            .await?
            .id)
    }

    pub async fn request_queue_size(&self, ch_id: &Uuid, min: i64) -> Result<usize> {
        Ok(sqlx::query_as::<_, TableCount>(r#"select count(*) as "count" from request where id > ?1 and channel = ?2"#)
            .bind(min)
            .bind(ch_id)
            .fetch_one(&self.con)
            .await?
            .count as usize)
    }

    pub async fn channels_last_request(&self, node_id: &str, last_id: i64) -> Result<Vec<ChannelLastRequest>> {
        Ok(sqlx::query_as::<_, ChannelLastRequest>(r#"select channel.id, max(request.id) as request_id from channel, request
where channel.id = request.channel and channel.node = ?1 and request.id > ?2
group by channel.id"#)
            .bind(node_id)
            .bind(last_id)
            .fetch_all(&self.con)
            .await?)
    }

    pub async fn channels<'a>(&'a self, node_id: &'a str) -> Pin<Box<dyn futures::stream::Stream<Item = Result<TaskChannel, sqlx::Error>> + Send + 'a>> {
        sqlx::query_as::<_, TaskChannel>(r#"select id from channel where node = ?1"#)
            .bind(node_id)
            .fetch(&self.con)
    }

    pub async fn user_playlists_count(&self, user: &Uuid) -> Result<usize> {
        Ok(sqlx::query_as::<_, TableCount>(r#"select count(*) as "count" from playlist where user = ?1"#)
            .bind(user)
            .fetch_one(&self.con)
            .await?
            .count as usize)
    }

    pub async fn add_playlist(&self, user: &Uuid, name: &str) -> Result<Uuid> {
        let mut seed: rand::rngs::StdRng = rand::SeedableRng::from_entropy();
        let id                           = Uuid::from_u128(seed.gen::<u128>());
        let time                         = chrono::offset::Utc::now().timestamp();
        
        sqlx::query(r#"insert into playlist values (?1, ?2, ?3, ?4)"#)
            .bind(user)
            .bind(id)
            .bind(name)
            .bind(time)
            .execute(&self.con)
            .await?;

        Ok(id)
    }

    pub async fn remove_user_playlist(&self, user: &Uuid, playlist: &Uuid) -> Result<()> {
        if sqlx::query(r#"delete from playlist where user = ?1 and id = ?2"#)
            .bind(user)
            .bind(playlist)
            .execute(&self.con)
            .await?
            .rows_affected() == 0 {
            Err(anyhow::Error::msg("Playlist doesn't exist"))
        } else {
            Ok(())
        }
    }

    pub async fn add_song_to_playlist(&self, user: &Uuid, playlist: &Uuid, song: &str) -> Result<Option<u32>> {
        match sqlx::query_as::<_, RowIdu32>(r#"insert into playlist_tracks values
(?1, ?2, ?3, (select coalesce(max(pos), 0) from playlist_tracks where user = ?1 and playlist = ?2) + 1) returning pos as "id""#)
            .bind(user)
            .bind(playlist)
            .bind(song)
            .fetch_one(&self.con)
            .await {
            Ok(v) => Ok(Some(v.id)),
            Err(sqlx::Error::Database(e)) => {
                if e.code() == Some(std::borrow::Cow::Borrowed("1555")) {
                    Ok(None)
                } else {
                    Err(e.into())
                }
            }
            Err(e) => Err(e.into())
        }
    }

    pub async fn remove_song_from_playlist(&self, user: &Uuid, playlist: &Uuid, pos: u32) -> Result<()> {
        let mut tx = self.con.begin().await?;
        if sqlx::query(r#"delete from playlist_tracks where user = ?1 and playlist = ?2 and pos = ?3"#)
            .bind(user)
            .bind(playlist)
            .bind(pos)
            .execute(&mut *tx)
            .await?
            .rows_affected() == 0 {
            return Err(anyhow::Error::msg("Song doesn't exist in playlist"));
        }

        sqlx::query(r#"update playlist_tracks set pos = pos - 1 where user = ?1 and playlist = ?2 and pos > ?3"#)
            .bind(user)
            .bind(playlist)
            .bind(pos)
            .execute(&mut *tx)
            .await?;

        tx.commit().await?;

        Ok(())
    }

    pub async fn update_song_pos_playlist(&self, user: &Uuid, playlist: &Uuid, pos: u32, new_pos: u32) -> Result<()> {
        let mut tx = self.con.begin().await?;
        // Here we are updating song position in whole playlist, we must account for:
        // - Change wanted song position
        // - Songs after new position: increment pos
        // - Songs after old position: decrement pos
        //
        // Old ordering:
        //  Songs names: A, B, C, D, E
        //  Songs poss : 1, 2, 3, 4, 5
        //
        // New ordering after chagins D's position to 2:
        //  Songs names: A, D, B, C, E
        //  Songs poss : 1, 2, 3, 4, 5
        //
        // Steps required to recalculate positions:
        // - Change position of D:    1, 2, 2, 3, 5
        // - Decrement after old_pos: 1, 2, 2, 3, 4
        // - Increment after new_pos: 1, 2, 3, 4, 5
        //
        //
        // Returning to original position by changing D's position to 4:
        //  Songs names: A, B, C, D, E
        //  Songs poss : 1, 2, 3, 4, 5
        //
        //  Steps:
        // - Change position of D:    1, 3, 4, 4, 5
        // - Decrement after old_pos: 1, 2, 3, 4, 4
        // - Increment after new_pos: 1, 2, 3, 4, 5
        let song = sqlx::query_as::<_, PlaylistSong>(r#"update playlist_tracks set pos = ?1 where user = ?2 and playlist = ?3 and pos = ?4 returning song, pos"#)
            .bind(new_pos)
            .bind(user)
            .bind(playlist)
            .bind(pos)
            .fetch_one(&self.con)
            .await?;

        sqlx::query(r#"update playlist_tracks set pos = pos - 1 where user = ?1 and playlist = ?2 and pos >= ?3 and song != ?4"#)
            .bind(user)
            .bind(playlist)
            .bind(pos)
            .bind(&song.song)
            .execute(&mut *tx)
            .await?;

        sqlx::query(r#"update playlist_tracks set pos = pos + 1 where user = ?1 and playlist = ?2 and pos >= ?3 and song != ?4"#)
            .bind(user)
            .bind(playlist)
            .bind(new_pos)
            .bind(&song.song)
            .execute(&mut *tx)
            .await?;

        tx.commit().await?;

        Ok(())
    }

    pub async fn playlist_songs_count(&self, user: &Uuid, playlist: &Uuid) -> Result<u32> {
        let count = sqlx::query_as::<_, TableCount>(r#"select coalesce(max(pos), 0) as "count" from playlist_tracks where user = ?1 and playlist = ?2"#)
            .bind(user)
            .bind(playlist)
            .fetch_one(&self.con)
            .await?
            .count;

        Ok(count)
    }

    pub async fn playlists(&self, user: &Uuid, song_not_present: Option<&String>) -> Result<Vec<Playlist>> {
        Ok(match song_not_present {
            Some(song_not_present) => sqlx::query_as::<_, Playlist>(r#"select name, id from playlist where user = ?1 and not exists
(select 1 from playlist_tracks where user = ?1 and playlist_tracks.playlist = playlist.id and playlist_tracks.song = ?2)
order by create_time desc"#)
                .bind(user)
                .bind(song_not_present)
                .fetch_all(&self.con),
            None => sqlx::query_as::<_, Playlist>(r#"select name, id from playlist where user = ?1 order by create_time desc"#)
                .bind(user)
                .fetch_all(&self.con)
        }.await?)
    }

    pub async fn playlist_songs<'a>(&'a self, user: &'a Uuid, playlist: &'a Uuid) -> Pin<Box<dyn futures::stream::Stream<Item = Result<PlaylistSong, sqlx::Error>> + Send + 'a>> {
        sqlx::query_as::<_, PlaylistSong>(r#"select song, pos from playlist_tracks where user = ?1 and playlist = ?2 order by pos desc"#)
            .bind(user)
            .bind(playlist)
            .fetch(&self.con)
    }
}
