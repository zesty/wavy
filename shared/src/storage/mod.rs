pub mod access;
pub mod media;

use sqlx::FromRow;

#[derive(FromRow, Debug)]
pub struct SchemaVersion {
    pub user_version: i64,
}

#[derive(FromRow, Debug)]
pub struct TableCount {
    pub count: u32,
}

#[derive(FromRow, Debug)]
pub struct TableHasRow {
    pub exists: bool,
}

#[derive(FromRow, Debug)]
pub struct RowId {
    pub id: i64,
}

#[derive(FromRow, Debug)]
pub struct RowIdu32 {
    pub id: u32,
}
